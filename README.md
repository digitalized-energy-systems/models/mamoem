# Mango-Mosaik Framework [readme-draft]

Mango-Mosaik is an implementation of a multi-agent system (mango) in a flexible Smart Grid co-simulation framework (mosaik).


## Installation

0. Install Poetry using the official instructions here: [Installing Poetry](https://python-poetry.org/docs/master/#installing-with-the-official-installer) if you haven’t done so already.

1. Install our dependencies using

    ```bash
    poetry install 
    ```

2. Create bare folders 'data' and 'logs' in your project folder if they were not contained in the repository.

3. For using simulators from midas it is necessary to configure/download them. (You don't need to do this if you have done this for a previous project using midas and the kept the configuration in the standard location.)

    ```bash
    poetry run midasctl configure
    ```

    and 

    ```bash
    poetry run midasctl download
    ```

And here you go you can run a scenario using:
    ```bash
    poetry run python scenario.py
    ```
or
    ```bash
    poetry shell
    python scenario.py
    ```

## Visualisation 

### Use "merit_order_plotly"

The script is used to visualize data from the database (../data/mosaik_results.hdf5). 

To do so it first uses the class DataExtraction in db_extract.py. The user needs to specify the parameters in db_extract.DataExtraction(...). An explanation of the necessary parameters can be found in the code.

Please be aware that (so far) the script expects you to know in which period the data is saved. If you don't know there is a workaround: Specify a period, and set the parameter "write_all_data_to_excel" to "True". If there's no data in that period you will get an error, but also there will be an "db_all_entries.xlsx" file in "/data" in which you can look up which are the periods with bids.

The extracted data is saved in a dataframe and visualized using plotly graph objects.
