import sys, inspect
import json
import pandas as pd
from dataclasses import dataclass, asdict
from typing import Dict, Iterable

# General messages
@dataclass
class TimedMessage:
    time_at_sender: int

@dataclass
class GeneralMessage:
    message_id: int
    converted_dict: Dict


# Messages of mango mosaik connection

@dataclass
class TimeMessage:
    """
    Contains current time in simulation as Unix timestamp
    """
    time_information: int

@dataclass
class MoTimeMessage(TimeMessage):
    market_connections: dict = None
    imbalance: float = None

@dataclass
class MpTimeMessage(TimeMessage):
    soc_percent: dict
    p_mw_forecast: dict

@dataclass
class AgentsTerminatedMessage:
    """
    Information about detected termination
    """

@dataclass
class RequestInformationMessage:
    """
    --> 1. RequestInformationMessage: MosaikAgent -> Mo/Mp
    2. AuctioninformationMessage: Mo/Mp -> MosaikAgent
    """
    requested_information: list

@dataclass
class AuctionInformationMessage:
    """
        1. RequestInformationMessage: MosaikAgent -> Mo/Mp
        --> 2. AuctioninformationMessage: Mo/Mp -> MosaikAgent
    """
    auction_id_type: list
    auction_id_tender_amount_supply_start: list
    auction_id_bids: list
    agent_data: dict = None


# Messages of mango agent market system

@dataclass
class RegistrationMessage:
    """
    To register market participants at grid operator
    """
    topic: str

@dataclass
class OpeningAuctionMessage(TimedMessage):
    """
    communicating (auction_product, supply_start, supply_duration), tender amount and gate closure time
    """
    auction_id: str
    product_type: str
    supply_start: int
    supply_duration: int
    tender_amount: int
    gate_opening_time: int
    gate_closure_time: int
    pricing_rule: str

@dataclass
class BidMessage(TimedMessage):
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    """
    auction_id: str
    bid_id: str
    amount_mw: int
    price: float

@dataclass
class MarketResultMessage(TimedMessage):
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    Todo: replace in code by BidMessage and appropriate usage of ACL performatives.
    """
    auction_id: str
    pricing_rule: str
    amount_mw: int
    marginal_price: float
    scenario: str = None


@dataclass
class AwardedAmountMessage(TimedMessage):
    """
    Containing bid: (auction_product, supply_start, supply_duration), amount, price
    Todo: replace in code by BidMessage and appropriate usage of ACL performatives.
    """
    auction_id: str
    bid_id: str
    amount_mw: int
    price: float = None
    scenario: str = None

@dataclass
class ActivationMessage(TimedMessage):
    """
    
    """
    auction_id: str
    product_type: str
    amount_mw: float

@dataclass
class AccountingMessage(TimedMessage):
    """
    
    """
    auction_id: str
    product_type: str
    supply_start: int
    supply_duration: int
    credit: float = None
    scenario: str = None

@dataclass
class CountMessagesMessage(TimedMessage):
    """
    Contains number of exchanged messages for termination detection
    """
    messages_in: int
    messages_out: int



def get_class(msg_id: int):
    if msg_id >= len(CLSMEMBERS):
        return None
    else:
        return CLSMEMBERS[msg_id]


def get_msg_id(msg_cls):
    if msg_cls not in CLSMEMBERS:
        return None
    else:
        return CLSMEMBERS.index(msg_cls)


def create_msg_content(cls_name, *args, **kwargs):
    # get id
    msg_id = get_msg_id(cls_name)
    if msg_id is None:
        return None
    else:
        return asdict(GeneralMessage(
            message_id=msg_id,
            converted_dict=asdict(cls_name(*args, **kwargs))
        ))


def read_msg_content(msg_dict: dict):
    """
    Check message content
    """
    if not isinstance(msg_dict, dict):
        return msg_dict
    else:
        if len(msg_dict.keys()) != 2 or 'message_id' not in msg_dict.keys() or 'converted_dict' not in msg_dict.keys():
            return msg_dict
        else:
            msg_cls = get_class(msg_dict['message_id'])
            return msg_cls(**msg_dict['converted_dict'])


CLSMEMBERS = [c for _, c in inspect.getmembers(sys.modules[__name__], inspect.isclass)]
