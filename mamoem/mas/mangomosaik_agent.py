import asyncio
from dataclasses import asdict
from typing import Tuple, Dict, Any
import random
from datetime import datetime
from queue import Queue

from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives

from mamoem.mas.agent_messages import *
from mamoem.util.logger import setup_logger
from mamoem.util.mango_mosaik_util import MessagesEnumerator

mosaik_logger = setup_logger('mas.mamo', 'logs/market.log')


class MaMoAgent(Agent):
    """
    A MaMoAgent is the general class (e.g. of market operators and market participants),
    which run in mosaik.
    It contains an optional message counting which is sent to the observer.
    """

    def __init__(self, container: Container, observer_addr: (str, int), observer_id: str,
                 seed: float=None):
        super().__init__(container)
        self.observer_addr = observer_addr
        self.observer_id = observer_id
        self.message_queue = Queue()
        self.random_generator = random.Random(seed)
        # print(self.aid, seed)

        self.current_time = -1
        self.msg_enumerator = MessagesEnumerator()  # for termination detection

    def handle_msg(self, content, meta: Dict[str, Any]):
        """Handling all incoming messages of the agent, 
        check time, (eventually put into message_queue,)
        call parent functions (for msg reading and counting).
        :param content: The message content
        :param meta: All meta information in a dict.
        """
        content, use_msg = self._begin_msg_handling(
            content=content, meta=meta)
        if use_msg: # use msg
            self._perform_msg_handling(content, meta)

            # work on earlier messages
            for _ in range(self.message_queue.qsize()):
                (content, meta) = self.message_queue.get()
                mosaik_logger.debug(f'From message queue: {content}, {meta}')
                if self._msg_time_from_present_or_past(content, meta):
                    self._perform_msg_handling(content, meta)
                else:
                    self.message_queue.put((content, meta))

            self._end_msg_handling()

        else: # store msg in message queue
            self.message_queue.put((content, meta))

    def _perform_msg_handling(self, content, meta: Dict[str, Any]):
        """
        From Agent.
        Has to be implemented by the user.
        """
        raise NotImplementedError

    def _begin_msg_handling(self, content, meta):
        """
        Should be called at the beginning of handle_msg in order to unravel and 
        count the arriving message.
        :param content: The message content.
        :param meta: All meta information in a dict.
        """
        self.msg_enumerator.increase_rcvd()
        content = read_msg_content(content)
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Received a message with content {content} and '
            f'meta {meta}. Current simulation time: {self.current_time}'
        )
        
        return content, self._msg_time_from_present_or_past(content, meta)

    def _msg_time_from_present_or_past(self, content, meta):
        if not hasattr(content, 'time_at_sender'):
            return True
        elif content.time_at_sender is None:
            return True
        elif content.time_at_sender > self.current_time:
            # print(f'{(self._container.addr, self.aid)}: Received a message from the future!')
            # print(f'Current time: {self.current_time}, received time: {content.time_at_sender}')
            # print(f'Content: {content}')
            mosaik_logger.warning(f'Agent {(self._container.addr, self.aid)}: '
                'The above message is received from the future! '
                'Current time: {self.current_time}, received time: {content.time_at_sender}')
            return False
        else:
            return True

    def _read_msg_content(content):
        return read_msg_content(content)

    def _perform_msg_handling(self, content, meta):
        raise NotImplementedError()

    def _end_msg_handling(self):
        """
        Should be called at the end of handle_msg in order to inform the observer
        about incoming and outgoing messages and reset the message counter for 
        further messages.
        """
        # Todo: Note, that Observer works only in simulations on single computer - not immediately
        #  on distributed clusters
        if self.observer_addr is not None: # only if observer is set
            self._inform_observer()
        self.msg_enumerator.reset()

    def _inform_observer(self):
        """
        Send current information about number of received and sent messages to observer.
        """
        if self.observer_addr is not None: # only needed, if observer is set
            content = create_msg_content(CountMessagesMessage, messages_in=self.msg_enumerator.get_rcvd(), 
                messages_out=self.msg_enumerator.get_sent(), time_at_sender=self.current_time
            )
            # mosaik_logger.debug(f'To observer {content}')
            self.schedule_instant_task(
                self._container.send_message(
                    receiver_addr=self.observer_addr,
                    receiver_id=self.observer_id,
                    content=content,
                    acl_metadata={'performative': Performatives.inform, 'conversation_id': None,
                                    'sender_id': self.aid},
                    create_acl=True,
                )
            )
