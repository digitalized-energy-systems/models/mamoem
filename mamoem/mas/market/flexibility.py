from amplify.src.flex_calculation import *
from mamoem.util.logger import setup_logger

flex_logger = setup_logger('mas.flex', 'logs/flexibility.log')
pp_logger = setup_logger('mas.flex.pp', 'logs/planning_problem.log')
setup_logger('amplify', "logs/amplify.log", propagate=False)

class FlexibilityDisposer:

    def __init__(self, label, p_max_grid_set_kw, cap_kwh, p_charge_max_kw, 
        p_discharge_max_kw, eta_ch, eta_dis, res: int = 15 * 60):
        self.label = label
        self.res = res
        
        # for flexibility handling of battery
        self.legacy_forecast = [1700, 2200, 2200, 2100, 1700, 1700, 2200, 1900]
        self.battery_flex = FlexCalculator(
            capacity_energy=cap_kwh*1e3, 
            max_p_bat=p_charge_max_kw*1e3,
            min_p_bat=-p_discharge_max_kw*1e3,  # minus sign!!!
            efficiency_charge=eta_ch, 
            efficiency_discharge=eta_dis,
            problem_detection_horizon= 2*60*60,
        )
        self.current_battery_flex = {}
        self.current_soc_percent = {}
        
        self.p_max_ps = p_max_grid_set_kw * 1e3 # W
        # self.mpos = [0.0]*8

    def update_soc(self, soc_percent_dict):
        """ Unravel sent forecast from ForecastDataModel.
        """
        # print(f'Received forecast of: {content.p_mw_forecast.values().values()} MW')
        
        for bat in soc_percent_dict:
            self.current_soc_percent[bat] = soc_percent_dict[bat]

            break # TODO: only first DER to not mix batteries.

    def update_forecast(self, forecast_dict):
        """ Unravel sent forecast from ForecastDataModel.
        """
        # print(f'Received forecast of: {content.p_mw_forecast.values().values()} MW')
        self.forecast = []
        for der in forecast_dict:
            for element in forecast_dict[der]:
                self.forecast.append(element * 1e6) # convert MW to W

            break # TODO: only first DER to not mix forecasts.

    def update_flexibility(self, mpos, current_time):
        # iterate over dict with DERs
        for bat in self.current_soc_percent:
            curr_soc = self.current_soc_percent[bat]/100
            # print(f'Current soc: {curr_soc}')
            flex_logger.debug(f'{self.label}: Simulation time: {current_time}, '
                f'current soc: {curr_soc}, P_max_grid_set: {self.p_max_ps}')
            # calculate flexibility
            self.current_battery_flex[bat] = self.battery_flex.calculate_total_flex(
                p_max_ps=[self.p_max_ps]*len(self.forecast), 
                mpos=mpos, forecast=self.forecast,
                curr_soc=curr_soc, avg_p_bat_of_curr_interval=0,
                passed_time_of_curr_interval=0, res=self.res, t_start=current_time
            )
            flex_logger.debug(f'{self.label}: Simulation time: {current_time}, '
                f'flexibility: {self.current_battery_flex[bat].flex_with_mpo}')
            if self.current_battery_flex[bat].problems:
                pp_logger.debug(f'{self.label}: Simulation time: {current_time}, '
                    f'{self.current_battery_flex[bat].problems}')
            else:
                flex_logger.debug(f'{self.label}: Simulation time: {current_time}: '
                    f'No planning problem')
        
        return self.current_battery_flex

    def determine_setpoints(self, mpos, current_time):
        flex = self.update_flexibility(mpos=mpos, current_time=current_time)
        output_setpoints = {}
        for bat in self.current_battery_flex:
            max = self.current_battery_flex[bat].flex_with_mpo.allowed_max_power[0]
            min = self.current_battery_flex[bat].flex_with_mpo.allowed_min_power[0]
            
            # setpoint = min+(max-min)*0.8
            setpoint = 0
            if max >= 0 and min <= 0: # no direction provided
                if self.current_soc_percent[bat] < 30:
                    setpoint = max # charge if too empty
                elif self.current_soc_percent[bat] > 70:
                    setpoint = min # discharge if too full
                else:
                    setpoint = 0
                # setpoint = 0
            elif max < 0:
                setpoint = max
            elif min > 0:
                setpoint = min
            output_setpoints[bat] = setpoint
            flex_logger.debug(f'{self.label}: Simulation time: {current_time}, forecast: {self.forecast}')
            flex_logger.debug(f'{self.label}: Simulation time: {current_time}, maximum power: {max}, minimum power: {min}, setpoint: {setpoint}')
        return output_setpoints

    def get_forecast_horizon_hours(self):
        return len(self.forecast) * self.res / 3600

    def get_forecast_horizon_index(self):
        return len(self.forecast)