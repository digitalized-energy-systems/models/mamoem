import sys, inspect
import json
import numpy
import pandas as pd
import math
from dataclasses import dataclass, asdict, field
from typing import Dict, Iterable, List

from mamoem.util.logger import setup_logger

mosaik_logger = setup_logger('mas.market_util', 'logs/market.log')

@dataclass
class Auction:
    product_type: str
    supply_start: int
    supply_duration: int
    tender_amount: float
    gate_opening_time: int
    gate_closure_time: int
    pricing_rule: str

    # bids: List[int] = field(default_factory=list)
    bids = pd.DataFrame(columns=["Bid_id", "Amount_MW", "Price_EUR", "Agent_addr", "Awarded_share"])
    winning_bids: List[int] = field(default_factory=list)
    loosing_bids: List[int] = field(default_factory=list)
    marginal_price: float = None

    status: str = "open"  # 'open', 'closed', 'cleared', 'accounted'

    def add_bid(self, agent, bid_id, amount_mw, price):
        # bid = pd.DataFrame({
        #     "Bid_id": bid_id, 
        #     "Amount_MW": amount_mw, 
        #     "Price_EUR": price,
        #     "Agent_addr": agent[0][0],
        #     "Agent_port": agent[0][1],
        #     "Agent_id": agent[1]}, 
        #     index=[0]
        # )
        bid = pd.DataFrame({
            "Bid_id": bid_id, 
            "Amount_MW": amount_mw, 
            "Price_EUR": price,
            "Agent_addr": [agent]}, 
            index=[0]
        )
        # print(bid)
        self.bids = pd.concat([self.bids, bid], axis=0, ignore_index=True)
        # print(self.bids)

        mosaik_logger.debug(f'Received and stored bid {bid} in: {self.bids}')

    def clear(self, tender_amount=None, bids=None):
        """
        Helper method to identify winning and loosing bids of an auction.
        :param tender_amount: The quantity tendered in the auction
        :param unsorted_bids: bids (sorted) and saved in a list
        """
        if tender_amount is None:
            tender_amount = self.tender_amount
        if bids is None:
            bids = self.bids

        # create new DataFrame with sorted price and new index (row numbers)
        sorted_bids = bids.sort_values(by=["Price_EUR"]).reset_index(drop=True)

        # add column "Cumulative amount (MW)" to find winning bids and 
        # market price
        sorted_bids["Cumulative_amount_MW"] = sorted_bids["Amount_MW"].cumsum()

        # loop over sorted_bids until tender_amount is reached
        tender_amount_reached = False
        self.marginal_price = 0
        for bidx in range(sorted_bids.shape[0]):
            if not tender_amount_reached:
                if bidx == 0:
                    free_amount = tender_amount
                else:
                    free_amount = tender_amount - \
                        sorted_bids["Cumulative_amount_MW"].iloc[bidx-1]
                sorted_bids.loc[bidx,"Awarded_share"] = \
                    min(1, free_amount/sorted_bids["Amount_MW"].iloc[bidx])
                self.marginal_price = sorted_bids["Price_EUR"].iloc[bidx]
                # stop calculation of awarded amount if tender_amount reached
                if sorted_bids["Cumulative_amount_MW"].iloc[bidx] >= tender_amount:
                    tender_amount_reached = True
            else:
                sorted_bids.loc[bidx,"Awarded_share"] = 0
                    
        mosaik_logger.debug(f'Cleared bids: {sorted_bids}')
        self.bids = sorted_bids
        return self.marginal_price

    def separate_mo_and_mp_bids(self, market_operators):
        winning_mos = pd.DataFrame(columns=["Bid_id", "Amount_MW", "Price_EUR", "Agent_addr", "Awarded_share"])
        winning_agents = pd.DataFrame(columns=["Bid_id", "Amount_MW", "Price_EUR", "Agent_addr", "Awarded_share"])
        for bidx in range(self.bids.shape[0]):
            bid = self.bids.iloc[bidx,0:5]
            agent_addr = self.bids["Agent_addr"].iloc[bidx]

            if agent_addr in market_operators:  # bid from market operator
                winning_mos.loc[winning_mos.shape[0]] = bid
                # if agent_addr in winning_mos:
                #     cumulated_mo_amount = winning_mos[agent_addr]
                #     winning_mos[agent_addr] = cumulated_mo_amount + self.bids["Amount_MW"].iloc[bidx]
                # else:
                #     winning_mos[agent_addr] = self.bids["Amount_MW"].iloc[bidx]
            else:  # bid from mp agent
                winning_agents.loc[winning_agents.shape[0]] = bid

        return winning_mos, winning_agents

@dataclass
class Bid:
    auction_id: str
    product_type: str
    power_flow_consumption: bool
    supply_start: int
    supply_duration: int
    amount_mw: float
    price: float
    probability: float = 1.0
    bid_id: str = None

class MarketManager:
    
    def __init__(self, label, cost: float, res: int = 15 * 60):
        self.label = label
        self.res = res
        self.cost = cost
        self.auctions = {}  # (br_product: str, supply_start: int, supply_duration: int): (tender_amount, gate_opening_time, gate_closure_time)
        self.bids = {}
        self.obligations = {}
        self.current_time = None

    ### simplyfied bidding
        self.last_place_bid_here = None
    ### end simplyfied bidding

        
    def add_bid(self, bid: Bid):
        self.bids[bid.auction_id] = bid

    ### simplyfied bidding
    def get_last_place_bid_here(self):
        return self.last_place_bid_here
    ### end simplyfied bidding

    def appoint_bids(self, flex):

        # generate bids
        new_bids = []
        # size bid, if closure time approaches
        if self.place_bid_here:
            auction = self.auctions[self.place_bid_here[0]]
            # decide on pos or neg control reserve bid
            start_index = time_to_index(
                self.current_time, auction.supply_start)
            end_index = time_to_index(
                self.current_time, 
                auction.supply_start + auction.supply_duration
            )
            if end_index > start_index + 1:
                raise ValueError
                pass # TODO: Implement longer bids
            else:
                charge_capacity = \
                    flex.allowed_max_power[start_index] - \
                    max(0, flex.allowed_min_power[start_index])
                discharge_capacity = abs(
                    flex.allowed_min_power[start_index] - \
                    min(0, flex.allowed_max_power[start_index])
                    )
                # flip directions of dis-/charge and pos/neg control capacity
                pos_control_capacity = discharge_capacity
                neg_control_capacity = charge_capacity
                # select auction to bid
                if len(self.place_bid_here) == 1:
                    bid_direction = auction.product_type
                    auction_id = self.place_bid_here[0]
                elif len(self.place_bid_here) == 2:
                    # choose direction
                    # based on power
                    if pos_control_capacity > neg_control_capacity:
                        bid_direction = 'positive_control_reserve'
                    elif pos_control_capacity < neg_control_capacity:
                        bid_direction = 'negative_control_reserve'
                    else:
                        # based on energy
                        max_energy = max(flex.allowed_max_energy_delta)
                        min_energy = min(flex.allowed_min_energy_delta)
                        if abs(min_energy) > abs(max_energy):
                            # more energy toward discharge direction
                            bid_direction = 'positive_control_reserve'
                        else:
                            # more energy toward charge direction
                            bid_direction = 'negative_control_reserve'
                    # select from multiple auctions
                    if bid_direction == 'positive_control_reserve':
                        # bid positive control reserve
                        if self.auctions[self.place_bid_here[0]].product_type == \
                            'positive_control_reserve':
                            auction_id = self.place_bid_here[0]
                        if self.auctions[self.place_bid_here[1]].product_type == \
                            'positive_control_reserve':
                            auction_id = self.place_bid_here[1]
                    if bid_direction == 'negative_control_reserve':
                        # bid negative control reserve
                        if self.auctions[self.place_bid_here[0]].product_type == \
                            'negative_control_reserve':
                            auction_id = self.place_bid_here[0]
                        if self.auctions[self.place_bid_here[1]].product_type == \
                            'negative_control_reserve':
                            auction_id = self.place_bid_here[1]
                auction = self.auctions[auction_id]
                # determine bid amount
                if bid_direction == 'positive_control_reserve':
                    bid_amount = pos_control_capacity
                    power_flow_consumption = False
                if bid_direction == 'negative_control_reserve':
                    bid_amount = neg_control_capacity
                    power_flow_consumption = True
            mosaik_logger.debug(f'{self.label}: Appoint bid amount: {bid_amount/1000} kW to {bid_direction} auction: {auction_id}')
            try:
                assert self.auctions[auction_id].supply_duration % 900 == 0
            except:
                mosaik_logger.debug(f'{self.label}: Supply duration: {self.auctions[auction_id].supply_duration}')
                return new_bids, False
            if bid_amount > 1e4:
                # if not power_flow_consumption:
                #     bid_amount = -bid_amount
                bid = Bid(
                    auction_id=auction_id,
                    product_type=auction.product_type,
                    power_flow_consumption=power_flow_consumption,
                    supply_start=auction.supply_start,
                    supply_duration=auction.supply_duration,
                    amount_mw=round_decimals_down(bid_amount / 1e6, 2),  # MW
                    price=self.cost
                )
                mosaik_logger.debug(f'{self.label}: New bid: {bid}')
                self.bids[auction_id] = bid
                new_bids.append(bid)

            ### simplyfied bidding
            self.last_place_bid_here = self.place_bid_here[0]
            ### end simplyfied bidding
            self.place_bid_here = []                

        return new_bids, False

    def add_obligation(self, auction_id, amount_mw):
        # move bid to obligation
        assert auction_id in self.bids.keys()
        bid = self.bids.pop(auction_id)
        if amount_mw > 0:
            bid.amount_mw = amount_mw
            self.obligations[auction_id] = bid
        mosaik_logger.debug(f'{self.label}: Obligations: {self.obligations}')

    def store_new_auction(self, auction_id, product_type: str, 
            supply_start: int, supply_duration: int, tender_amount: int,
            gate_opening_time: int, gate_closure_time: int, pricing_rule: str):

        # store open auction locally
        self.auctions[auction_id] = Auction(
            product_type=product_type,
            supply_start=supply_start,
            supply_duration=supply_duration,
            tender_amount=tender_amount,
            gate_opening_time=gate_opening_time,
            gate_closure_time=gate_closure_time,
            pricing_rule=pricing_rule
        )
        mosaik_logger.debug(f'{self.label}: Stored new auction: {self.auctions}')

    def update_auctions(self, current_time):
        """Loop over all remembered auctions and delete those overdue
        or mark 'closed' ones.
        """
        self.current_time = current_time
        self.place_bid_here = []
        deletable_auctions = []
        for auction_id in self.auctions:
            # mark auction closed
            if self.auctions[auction_id].status == 'open' and \
                current_time >= self.auctions[auction_id].gate_closure_time:
                # if closure time due
                self.auctions[auction_id].status = 'closed'
            
            # find next auction to close and mark for bidding here
            if self.auctions[auction_id].status == 'open' and \
                self.auctions[auction_id].gate_closure_time - \
                current_time <= 900:
                self.place_bid_here.append(auction_id)

            # mark for deletion
            supply_end = self.auctions[auction_id].supply_start + \
                self.auctions[auction_id].supply_duration
            if current_time >= supply_end:
                # if product was already provided
                deletable_auctions.append(auction_id)

        # delete auctions
        for auction_id in deletable_auctions:
            self.auctions.pop(auction_id)

        mosaik_logger.debug(f'{self.label}: Place bid here: {self.place_bid_here}')

    def update_bids(self, current_time):
        """Remove bid from self.bids 
        - if provision lies in the past
        - if gate closure lies more than 15 minutes in the past and 
          bid was not accepted.
        """
        self.current_time = current_time
        deletable_bids = []
        for auction_id in self.bids:
            bid = self.bids[auction_id]
            if current_time > bid.supply_start + bid.supply_duration:
                deletable_bids.append(auction_id)

        for auction_id in deletable_bids:
            self.bids.pop(auction_id)
        mosaik_logger.debug(f'{self.label}: Bids: {self.bids}')

    def get_assembled_mpo_list(self, forecast_horizon_index: int):
        # assemble one mpo list from pos/neg mpo vectors
        
        pos_mpos, neg_mpos = self.get_mpo_vectors(forecast_horizon_index)

        mpos = [0.0]*max(len(pos_mpos), len(neg_mpos))
        for index in range(len(pos_mpos)):
            if pos_mpos[index] > 0:
                assert neg_mpos[index] == 0
                mpos[index] = pos_mpos[index]
            elif neg_mpos[index] < 0:
                assert pos_mpos[index] == 0
                mpos[index] = neg_mpos[index]
        return mpos

    def get_mpo_vectors(self, forecast_horizon_index: int):

        pos_mpos = numpy.zeros(forecast_horizon_index)
        neg_mpos = numpy.zeros(forecast_horizon_index)
        all_bids = list(self.obligations.values()) + list(self.bids.values())
        # print(all_bids)
        for bid in all_bids:
            # determine start and (exluded) end of bid
            start_index = time_to_index(self.current_time, bid.supply_start)
            end_index = time_to_index(
                self.current_time, bid.supply_start + bid.supply_duration)
            # print(f'Start: {start_index}, end: {end_index}')
            if end_index < 0:
                continue  # do not use bids from the past
            if start_index < 0:
                start_index = 0  # start at the beginning of the list
            # access bids within the forecast horizon
            if start_index <= forecast_horizon_index:
                if bid.power_flow_consumption:  # direction
                    pos_mpos[start_index:end_index] = \
                        numpy.add(pos_mpos[start_index:end_index], bid.amount_mw*1e6)
                else:
                    neg_mpos[start_index:end_index] = \
                        numpy.add(neg_mpos[start_index:end_index], -abs(bid.amount_mw)*1e6)
        # print(f'pos_mpos: {pos_mpos}')
        # print(f'neg_mpos: {neg_mpos}')
        return pos_mpos, neg_mpos

def time_to_index(current_time: int, future_time: int, res = 900):
    """Calculate the index of a vector based future point in time."""
    time_difference = future_time - current_time
    offset_from_current_interval = current_time % res
    return int((time_difference + offset_from_current_interval) / res)


def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return math.floor(number)

    factor = 10 ** decimals
    return math.floor(number * factor) / factor