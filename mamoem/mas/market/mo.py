import asyncio
from dataclasses import asdict
from typing import Tuple, Dict, Any
from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives
from mamoem.mas.mangomosaik_agent import MaMoAgent
from mamoem.mas.agent_messages import *
from mamoem.mas.market.market_util import *
from datetime import datetime
import uuid

from ast import literal_eval as make_tuple

mosaik_logger = setup_logger('mas.mo  ', 'logs/market.log')

class MoAgent(MaMoAgent):
    """
    A MoAgent is the Market Operator and organizes the auctions.
    :param mp_agents: {(host,port),id} : [br_product1, br_product2, ...] Dict of lists, of which the keys are mp addrs
    :param downstream_mos: {((host,port),id) : 'all' Dict of lists, of which the keys are downstream mo addrs and each
    list contains only 'all' at first
    :param received_bids: (br_product: str, supply_start: int, supply_duration: int): [bid1, bid2, ...] Dict of lists,
    of which the keys are the identifier of one auction
    :param open_auctions: (br_product: str, supply_start: int, supply_duration: int):
    (tender_amount: int, gate_opening_time: int, gate_closure_time: int, pricing_rule: str)
    :param auctions: {auction_id: Auction}
    """

    def __init__(self, container: Container, upstream_mos=None, observer_addr=None, observer_id=None,
                 seed=0):
        super().__init__(container=container, observer_addr=observer_addr, observer_id=observer_id, seed=seed)
        self.upstream_mos = upstream_mos
        self.mp_agents = {}
        self.downstream_mos = {}

        self.received_bids = {}  #

        self.open_auctions = {}
        self.auctions = {}
        self.bid_counter = 0

        # attributes to be saved in database
        # TODO: check whether attributes can be taken from "auctions" or "open_auctions" variables
        self.tender_amount: int
        self.auction_id: str
        self.supply_start: int

        self.db_auction_id_type = []
        self.db_auction_id_tender_amount_supply_start = []
        self.db_auction_id_bids = []

        self._market_connections = {} # {((host,port),id) : Dict of bool, whether connection to upstream mo is established (the keys are upstream mo addrs)

    @classmethod
    async def create(cls, container: Container, upstream_mos=None, observer_addr=None,
                     observer_id=None, seed=0):
        """Return a new :class:`MoAgent` instance.

        *main_container* is the main_container that the agent lives in.

        """
        # We use a factory function here because __init__ cannot be a coroutine
        # and we want to make sure the agent is registered at the controller and that
        # when we return the instance we have have a fully initialized
        # instance.
        #
        # Classmethods don't receive an instance "self" but the class object as
        # an argument, hence the argument name "cls".

        mo = cls(container=container, upstream_mos=upstream_mos,
                 observer_addr=observer_addr, observer_id=observer_id,
                 seed=seed)

        return mo

    # def handle_msg(self, content, meta: Dict[str, Any]):
    #     """Handling all incoming messages of the agent, 
    #     check time, (eventually put into message_queue,)
    #     call parent functions (for msg reading and counting).
    #     :param content: The message content
    #     :param meta: All meta information in a dict.
    #     """
    #     content, use_msg = self._begin_msg_handling(
    #         content=content, meta=meta)
    #     if use_msg: # use msg
    #         self._perform_msg_handling(content, meta)

    #         # work on earlier messages
    #         for _ in range(self.message_queue.qsize()):
    #             (content, meta) = self.message_queue.get()
    #             mosaik_logger.debug(f'From message queue: {content}, {meta}')
    #             if self._msg_time_from_present_or_past(content, meta):
    #                 self._perform_msg_handling(content, meta)
    #             else:
    #                 self.message_queue.put((content, meta))

    #         self._end_msg_handling()

    #     else: # store msg in message queue
    #         self.message_queue.put((content, meta))
            
    def _perform_msg_handling(self, content, meta):
        """Perform handling of messages, which means 
        activate specific message handling.
        :param content: The message content
        :param meta: All meta information in a dict.
        """
        if isinstance(content, TimeMessage):

            if content.time_information is None:
                # registration
                self._register_at_upstream_mos()
            else:  # normal operation
                self.current_time = content.time_information
                self.current_imbalance = content.imbalance
                self._update_market_connections(content.market_connections)
                # check for regular tasks
                self._perform_regular_tasks()

        if isinstance(content, RegistrationMessage):
            self._handle_registration_msg(content=content, meta=meta)

        if isinstance(content, OpeningAuctionMessage):
            self._handle_gate_opening_msg(content=content, meta=meta)

        if isinstance(content, BidMessage):
            self._handle_bid_msg(content=content, meta=meta)

        if isinstance(content, MarketResultMessage):
            self._handle_market_result_msg(content=content, meta=meta)

        if isinstance(content, ActivationMessage):
            self.handle_activation_msg(content, meta)
            
        if isinstance(content, RequestInformationMessage):
            self.handle_request_information_msg(content, meta)

        # self._end_msg_handling()

    def _perform_regular_tasks(self):
        """Perform tasks, which are needed or checked every time step.
        """
        # activate needed reserves
        if self.current_imbalance != 0.0:
            self._activate_control_reserve()
        # check self.open_auctions for due closure and accounting time
        clearable_auctions = []
        accountable_auctions = []
        for auction_id in self.auctions:
            # only highest MO starts opening and clearing of auctions
            if not self.upstream_mos:
                if self.auctions[auction_id].status == 'open' and \
                    self.current_time >= self.auctions[auction_id].gate_closure_time:
                    # if closure time due
                    clearable_auctions.append(auction_id)
                    self.auctions[auction_id].status = 'closed'

            supply_end = self.auctions[auction_id].supply_start + \
                self.auctions[auction_id].supply_duration
            if self.auctions[auction_id].status == 'cleared' and \
                self.current_time >= supply_end:
                # if product was already provided
                accountable_auctions.append(auction_id)

        # accounting is started locally
        self._begin_accounting(accountable_auctions)

        # only highest MO starts opening and clearing of auctions
        if not self.upstream_mos:
            self._begin_clear_auctions(clearable_auctions)
            opened_auctions = self._start_auctions()
            # TODO:
            self._inform_connected_agents_about_gate_opening(opened_auctions)

    def _activate_control_reserve(self):
        # positive reserve needed
        if self.current_imbalance < 0:
            for auction_id in self.auctions:
                if self.current_time == \
                    self.auctions[auction_id].supply_start and \
                    self.auctions[auction_id].product_type == \
                    'positive_control_reserve':
                    self._activate(auction_id)
        # negative reserve needed
        elif self.current_imbalance > 0:
            for auction_id in self.auctions:
                if self.current_time == \
                    self.auctions[auction_id].supply_start and \
                    self.auctions[auction_id].product_type == \
                    'negative_control_reserve':
                    self._activate(auction_id)
            
    def handle_activation_msg(self, content, meta):
        if self.current_imbalance == 0.0:
            self._activate(
                auction_id=content.auction_id,
                amount_mw=content.amount_mw
            )

    def _activate(self, auction_id, amount_mw=None):

        if amount_mw is None: # for most upstream mo
            imbalance = self.current_imbalance
        else:
            bids = self.auctions[auction_id].bids
            awarded_amount = bids[["Amount_MW","Awarded_share"]].prod(1).sum()
            mosaik_logger.debug(f'Awarded amount by upstream mo: {awarded_amount}')
            imbalance = amount_mw / awarded_amount
        # equal share of all bids
        
        downstream_mo_bids, winning_bids = \
            self.auctions[auction_id].separate_mo_and_mp_bids(
                self.downstream_mos
            )

        # market operators
        # for mo_addr in downstream_mo_bids["Agent_addr"].unique(): # this line works safely
        for mo_addr in self.downstream_mos.keys():
            agents = downstream_mo_bids["Agent_addr"]==mo_addr
            # multiply awarded share with imbalance and sum up
            amount_mw = downstream_mo_bids["Amount_MW"].loc[agents].mul(
                downstream_mo_bids["Awarded_share"].loc[agents]
            ).sum() * imbalance,
            amount_mw = amount_mw[0]
            # print(f'Amount in MW: {amount_mw}')
            if amount_mw != 0.0:
                content = create_msg_content(
                    ActivationMessage, 
                    time_at_sender=self.current_time, 
                    auction_id=auction_id,
                    product_type=self.auctions[auction_id].product_type, 
                    amount_mw=amount_mw
                )
                agent = {mo_addr: None}
                self._inform_agents(agent, content, performative=Performatives.accept_proposal)

        # market participants
        messages_dict = {}
        for bidx in range(winning_bids.shape[0]):
            bid = winning_bids.iloc[bidx]
            if bid["Awarded_share"] > 0:
                amount_mw = bid["Amount_MW"] * bid["Awarded_share"] * imbalance
                content = create_msg_content(
                    ActivationMessage, 
                    time_at_sender=self.current_time,
                    auction_id=auction_id,
                    product_type=self.auctions[auction_id].product_type, 
                    amount_mw=amount_mw
                )
                messages_dict[bid["Agent_addr"]] = content

        self._inform_agents_dict(messages_dict=messages_dict, performative=Performatives.accept_proposal)

    def _update_market_connections(self, market_connections):
        """Read market connection dict and store locally.
        """
        if market_connections is not None:
            for mo in market_connections: # parse tuple from transmitted string
                mt = mo
                rm_chars = "()'"
                for char in rm_chars:
                    mt = mt.replace(char,"")
                mt = mt.split(',')

                mt = ((mt[0], int(mt[1])), mt[2])
                if mt not in self._market_connections.keys():
                    self._market_connections[mt] = {}
                self._market_connections[mt][self.current_time] = market_connections[mo]

    def _inform_agents(self, agents, content, performative=Performatives.inform, conv_id=None):
        """Send message from content to all agents in the list.
        :param agents: a list with all receiving agents: [(addr1, id1), (addr2, id2), ...]
        :param content: The message content
        :param performative: the ACL performative from the Performatives enumeration
        :param conv_id: a conversation id
        """
        for addr, aid in agents:
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Send message to addr: {addr}, aid: {aid}')
            self.schedule_instant_task(
                self._container.send_message(
                    receiver_addr=addr,
                    receiver_id=aid,
                    content=content,
                    acl_metadata={'performative': performative, 'conversation_id': conv_id,
                                  'sender_id': self.aid},
                    create_acl=True,
                )
            )
            self.msg_enumerator.increase_sent()
    
    def _inform_agents_dict(self, messages_dict, performative=Performatives.inform, conv_id=None):
        """Send message from content to all agents in the list.
        :param agents: a list with all receiving agents: [(addr1, id1), (addr2, id2), ...]
        :param content: The message content
        :param performative: the ACL performative from the Performatives enumeration
        :param conv_id: a conversation id
        """
        for addr, aid in messages_dict:
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Send message to addr: {addr}, aid: {aid}')
            self.schedule_instant_task(
                self._container.send_message(
                    receiver_addr=addr,
                    receiver_id=aid,
                    content=messages_dict[(addr, aid)],
                    acl_metadata={'performative': performative, 'conversation_id': conv_id,
                                  'sender_id': self.aid},
                    create_acl=True,
                )
            )
            self.msg_enumerator.increase_sent()
    
    def _start_auctions(self):
        opened_auctions = []
        # start auction every quarterhour one hours ahead of delivery
        opened_auctions.append(
            self._open_control_reserve_auction(
                current_time=self.current_time, tender_amount=0.3,
                gate_opening_time=self.current_time,
                gate_closure_time=self.current_time + 1805,
                product_type='positive_control_reserve'
            )
        )
        opened_auctions.append(
            self._open_control_reserve_auction(
                current_time=self.current_time, tender_amount=0.3,
                gate_opening_time=self.current_time,
                gate_closure_time=self.current_time + 1805,
                product_type='negative_control_reserve'
            )
        )
        return opened_auctions

    def _open_control_reserve_auction(self, current_time: int,
        tender_amount: int, gate_opening_time: int, gate_closure_time: int,
        product_type: str):
        """Is called after auction has been scheduled. According to current
        time defines an auction with tender amount,
        gate opening time und gate closure time as parameters.
        :param current_time: The current time in the simulation
        :param tender_amount: The quantity tendered in the auction
        :param gate_opening_time: Trading is open from this time on for this auction
        :param gate_closure_time: Trading is closed on this time for this auction
        """

        auction_id = str(uuid.uuid4())
        # print(type(auction_id))
        # some values for functional supply start and duration
        supply_start = current_time + 3600
        supply_duration = 900
        pricing_rule = 'uniform_pricing'
        self.auctions[auction_id] = Auction(
            product_type=product_type,
            supply_start=supply_start, supply_duration=supply_duration,
            tender_amount=tender_amount, gate_opening_time=gate_opening_time,
            gate_closure_time=gate_closure_time, pricing_rule=pricing_rule
        )
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Created auction {auction_id}: {self.auctions[auction_id]}')
        return auction_id

    def _inform_connected_agents_about_gate_opening(self, opened_auctions:list):
        """Method of upstream and downstream mo to distribute opened GateOpeningMessage
        as auction_data to connected agents (mos and mps).
        :param auction_identifier: Key of open auction
        :param auction_data: contains further parameters of the auction
        """
        for auction_id in opened_auctions:
            auction = self.auctions[auction_id]
            content = create_msg_content(
                OpeningAuctionMessage, 
                time_at_sender=self.current_time, auction_id=auction_id, 
                product_type=auction.product_type, supply_start=auction.supply_start,
                supply_duration=auction.supply_duration,
                tender_amount=auction.tender_amount,
                gate_opening_time=auction.gate_opening_time,
                gate_closure_time=auction.gate_closure_time, 
                pricing_rule=auction.pricing_rule
            )

            # Inform connected market participants
            self._inform_agents(self.mp_agents, content)

            # Inform downstream market operators
            self._inform_agents(self.downstream_mos, content)

    def _handle_gate_opening_msg(self, content, meta):
        """Method of downstream mo which receives GateOpeningMessage, opens it and stores
        it locally, before distributing it to connected agents (mos and mps).
        :param content: Message content of opened auction
        :param meta: All meta information in a dict.
        """
        self.auctions[content.auction_id] = Auction(
            product_type=content.product_type,
            supply_start=content.supply_start,
            supply_duration=content.supply_duration,
            tender_amount=content.tender_amount,
            gate_opening_time=content.gate_opening_time,
            gate_closure_time=content.gate_closure_time,
            pricing_rule=content.pricing_rule
        )

        self._inform_connected_agents_about_gate_opening([content.auction_id])

    def _begin_clear_auctions(self, clearable_auctions: list):
        """Looping through all clearable auctions and call _clear auction.
        :param current_time: The current time in the simulation
        """
        for auction_id in clearable_auctions:
            self._clear_auction(auction_id)           

        # print('Open auctions: ', self.open_auctions)
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Open Auctions: {self.open_auctions}, Current Time: {self.current_time} ')

    def _clear_auction(self, auction_id, tender_amount = None, marginal_price = None):
        """Loop through received bids, sort and award amounts and inform winners and losers. 
        Uniform-pricing and pay-as-bid pricing implemented.
        :param auction_id: The to be cleared auction 
        """
        auction = self.auctions[auction_id]
        if tender_amount is None:
            tender_amount = auction.tender_amount
        else:
            self.auctions[auction_id].tender_amount = tender_amount

        mosaik_logger.debug(
            f'Agent {(self._container.addr, self.aid)}: '
            f'END AUCTION: ID: {auction_id}, TA {auction.tender_amount}, '
            f'GateOpen {auction.gate_opening_time}, '
            f'GateClose {auction.gate_closure_time}'
        )
        # perform auction clearing
        auction.clear(tender_amount=tender_amount)
        
        # if marginal_price is not None:
        #     assert marginal_price == self.auctions[auction_id].winning_bids[-1][2]

        # inform winning agents and connected market operators
        self._inform_winning_mps_and_mos(auction_id=auction_id)

        # inform loosers
        self._inform_loosing_mps(auction_id=auction_id)
        self.auctions[auction_id].status = 'cleared'

        # for database output
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'values: {self.auctions.values()}')
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'auction_id_bids: {self.db_auction_id_bids}')
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'auction_id_type: {self.db_auction_id_type}')
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'auction_id_tender_amount_supply_start: '
            f'{self.db_auction_id_tender_amount_supply_start}')
        
        # save auction_id in both lists for database
        self.db_auction_id_type.append(auction_id)
        self.db_auction_id_tender_amount_supply_start.append(auction_id)
        self.db_auction_id_bids.append(auction_id)
        # append further attributes to respective lists
        # get variables from "auction object" in self.auctions
        product_type = getattr(self.auctions[auction_id], 'product_type')
        tender_amount = getattr(self.auctions[auction_id], 'tender_amount')
        supply_start = getattr(self.auctions[auction_id], 'supply_start')
        # append type
        self.db_auction_id_type.append(product_type)
        # append tender_amount and supply_start to local variable
        self.db_auction_id_tender_amount_supply_start.append(tender_amount)
        self.db_auction_id_tender_amount_supply_start.append(supply_start)
        # get bids from self.auctions
        bids = getattr(self.auctions[auction_id], 'bids')
        mosaik_bids = []
        # print(bids)
        for bidx in range(bids.shape[0]):
            mosaik_bids.append(
                (
                bids["Agent_addr"].loc[bidx], 
                bids["Amount_MW"].loc[bidx], 
                bids["Price_EUR"].loc[bidx], 
                bids["Bid_id"].loc[bidx],
                bids["Awarded_share"].loc[bidx],
                )
            )
            # print(mosaik_bids)
        # append bids to local variable
        self.db_auction_id_bids.append(mosaik_bids)

    def _inform_winning_mps_and_mos(self, auction_id):
        """Method of upstream and downstream mo in order to inform connected mps about winning bids.
        Here, the AwardedAmountMessage is assembled and sent.
        :param auction_id: Key of auction (has to be in self.auctions)
        :param downstream_winning_mos: Dict with key (add, aid) containing mps and the low priced bids from them
        :param marginal_price: marginal price of auction
        """
        
        # # MPs:
        # downstream_winning_mos = self._inform_winning_mps(auction_id=auction_id)
        # # MOs:
        # self._inform_winning_mos(
        #     auction_id=auction_id, downstream_winning_mos=downstream_winning_mos
        # )

        downstream_mo_bids, winning_bids = \
            self.auctions[auction_id].separate_mo_and_mp_bids(self.downstream_mos)
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Downstream winning MOs: {downstream_mo_bids}')
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Winning MPs: {winning_bids}')
        # print(downstream_mo_bids)
        # print(winning_bids)
            # if bid[0] in self.downstream_mos:  # bid from downstream mos
            #     if bid[0] in downstream_winning_mos:
            #         count = downstream_winning_mos[bid[0]]
            #         mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            #             f'Count: {count}, DWM: {downstream_winning_mos}')
            #         downstream_winning_mos[bid[0]] = count + bid[1]
            #     else:
            #         downstream_winning_mos[bid[0]] = bid[1]
            #     # TODO: Here insert logic for possibly increased local tender amount.
            #     mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            #         f'DWM: {downstream_winning_mos}')


        price = self.auctions[auction_id].marginal_price
        # market operators
        # for mo_addr in downstream_mo_bids["Agent_addr"].unique(): # this line works safely
        for mo_addr in self.downstream_mos.keys():

            pricing_rule = self.auctions[auction_id].pricing_rule
            agents = downstream_mo_bids["Agent_addr"]==mo_addr
            # multiply with awarded share and sum up
            amount_mw = downstream_mo_bids["Amount_MW"].loc[agents].mul(
                downstream_mo_bids["Awarded_share"].loc[agents]
            ).sum(),
            amount_mw = amount_mw[0]
            # print(f'Amount in MW: {amount_mw}')
            
            content = create_msg_content(
                MarketResultMessage, 
                time_at_sender=self.current_time, auction_id=auction_id,
                pricing_rule=pricing_rule, amount_mw=amount_mw,
                marginal_price=self.auctions[auction_id].marginal_price, 
                scenario='mandatory islanding'
            )
            agent = {mo_addr: None}
            self._inform_agents(agent, content, performative=Performatives.accept_proposal)

        # market participants
        messages_dict = {}
        for bidx in range(winning_bids.shape[0]):
            bid = winning_bids.iloc[bidx]
            # if self.auctions[auction_id].pricing_rule == 'uniform_pricing':
            #     price = marginal_price
            if self.auctions[auction_id].pricing_rule == 'pay_as_bid_pricing':
                price = bid["Price_EUR"]

            assert price != None
            content = create_msg_content(
                AwardedAmountMessage, 
                time_at_sender=self.current_time,
                auction_id=auction_id,
                bid_id=bid["Bid_id"],
                amount_mw=bid["Amount_MW"] * bid["Awarded_share"], 
                price=price, scenario='mandatory islanding'
            )
            messages_dict[bid["Agent_addr"]] = content

                # agent = {(bid[0][0], bid[0][1]): None}
        self._inform_agents_dict(messages_dict=messages_dict, performative=Performatives.accept_proposal)

                # mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                #     f'Awarded bid: {bid}, Content: {content}')
        return downstream_mo_bids

    def _inform_winning_mos(self, auction_id, downstream_winning_mos):
        """Method of upstream and downstream mo in order to inform downstream mos about winning bids from them.
        Here, the MarketResultMessage is assembled and sent.
        :param auction_id: Key of auction (has to be in self.auctions)
        :param downstream_winning_mos: Dict with key (add, aid) containing mos and the low priced bids from them
        :param marginal_price: marginal price of auction
        """
        # message to downstream mos
        # print('Downstream winning mos: ', downstream_winning_mos)
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Downstream winning mos: {downstream_winning_mos}')
        for addr, aid in downstream_winning_mos:
            amount_mw = downstream_winning_mos[(addr, aid)]
            pricing_rule = self.auctions[auction_id].pricing_rule
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Addr: {addr}, ID: {aid}, Amount: {amount_mw}')
            
            content = create_msg_content(
                MarketResultMessage, 
                time_at_sender=self.current_time, auction_id=auction_id,
                pricing_rule=pricing_rule, amount_mw=amount_mw, 
                marginal_price=self.auctions[auction_id].marginal_price, 
                scenario='mandatory islanding'
            )
            agent = {(addr, aid): None}
            self._inform_agents(agent, content, performative=Performatives.accept_proposal)

    def _inform_loosing_mps(self, auction_id):
        for bid in self.auctions[auction_id].loosing_bids:
            if bid[0] not in self.downstream_mos:
                content = create_msg_content(
                    AwardedAmountMessage, 
                    time_at_sender=self.current_time, auction_id=auction_id,
                    amount_mw=0, scenario='mandatory islanding'
                )
                agent = {(bid[0][0], bid[0][1]): None}
                self._inform_agents(agent, content, performative=Performatives.reject_proposal)

    def _handle_market_result_msg(self, content, meta):
        """Method of downstream mo in order to trigger local market clearing and remove open auction.
        :param content: Message content of market result message
        :param meta: All meta information in a dict.
        """
        self._clear_auction(
            content.auction_id,
            tender_amount=max(content.amount_mw, 0.2),  # local tender amount
            # tender_amount=content.amount_mw,   # from upstream mo
            marginal_price=content.marginal_price
        )

    def handle_request_information_msg(self, content, meta: Dict[str, any]):
        """To send information to mosaik database.
        """
        # get information from the meta dict
        list_addr = meta.get('sender_addr', None)
        aid = meta.get('sender_id', None)
        conversation_id = meta.get('conversation_id', None)

        if list_addr is not None and aid is not None:
            out_content = create_msg_content(AuctionInformationMessage,
                auction_id_type=self.db_auction_id_type,
                auction_id_tender_amount_supply_start=
                self.db_auction_id_tender_amount_supply_start,
                auction_id_bids=self.db_auction_id_bids
            )
            # print(f'Addr: {list_addr}, type: {type(list_addr)}; Id: {aid}, type: {type(aid)}')
            agent = {((list_addr[0], list_addr[1]), aid): None}
            self._inform_agents(agent, out_content, conv_id=conversation_id)

            # clear db_auction_id_tender_amount_supply_start and db_auction_id_bids to avoid cluttering DB
            self.db_auction_id_type = []
            self.db_auction_id_tender_amount_supply_start = []
            self.db_auction_id_bids = []

    def _handle_bid_msg(self, content, meta: Dict[str, Any]):
        """This function handles the incoming bids and checks if the content is valid 
        (deadline not passed, right format).
        It either sends a deny for invalid messages or stores them into received_bids.

        :param content: The content of the SELL message
        :type content: :class:`fmm.protomsg.fmm_message_pb2.FMM_Message`
        :param meta: All meta information in a dict.
        """
        auction_id = content.auction_id
        if auction_id in self.auctions:
            if self._deadline_valid(auction_id):
                
                # store bid in received bids in self.auction[id].bids
                self.auctions[auction_id].add_bid(
                    agent=(meta['sender_addr'], meta['sender_id']),
                    bid_id=content.bid_id,
                    amount_mw=content.amount_mw, 
                    price=content.price
                )

                # TODO: Unterscheiden, an welches topic die Gebote geschickt werden
                # send bid to upstream mo
                mosaik_logger.debug(
                    f'Agent {(self._container.addr, self.aid)}: '
                    'Upstream MO set: {}'.format(self.upstream_mos != None)
                )
                if self.upstream_mos: # send bids to upstream mo
                    for upstream_mo in self.upstream_mos:
                        out_content = create_msg_content(
                            BidMessage, 
                            time_at_sender=self.current_time,
                            auction_id=auction_id,
                            bid_id=content.bid_id,
                            amount_mw=content.amount_mw, 
                            price=content.price)
                        self.bid_counter += 1
                        agent = {(upstream_mo[0], upstream_mo[1]): None}
                        self._inform_agents(agent, out_content)
            else:
                # TODO: Rückantwort o.ä. schicken
                print("Denied")

    def _deadline_valid(self, auction_id):
        """Checking whether the deadline of an auction has not yet been reached.
        :param content: The message content
        """
        # TODO: integrate into Auction.add_bid() together with additional functionality like check for right price range etc.
        gate_closure_time = self.auctions[auction_id].gate_closure_time
        if self.current_time < gate_closure_time:
            return True
        else:
            return False
    
    def _begin_accounting(self, accountable_auctions: list):
        """Loop through accountable_auctions.
        Account remuneration to an agent, who won an auction, after the delivery
        of the purchased service (supply start/end and simulation time are respected).
        """
        # TODO: move accounting into Auction object
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Begin auction accounting: {accountable_auctions}')
        for auction_id in accountable_auctions:
            auction = self.auctions[auction_id]
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Accountable bids: {auction.winning_bids}')
            for bid in auction.winning_bids: # iterate winning bids
                if self.auctions[auction_id].pricing_rule == 'uniform_pricing':
                    credit = bid[1] * self.auctions[auction_id].marginal_price
                elif self.auctions[auction_id].pricing_rule == 'pay_as_bid_pricing':
                    credit = bid[1]*bid[2]

                content = create_msg_content(AccountingMessage, time_at_sender=self.current_time,
                    auction_id=auction_id,
                    product_type=auction.product_type, supply_start=auction.supply_start,
                    supply_duration=auction.supply_duration, credit = credit, 
                    scenario='mandatory islanding'
                ) # account amount multiplied with price
                # send message:
                agent = {(bid[0][0], bid[0][1]): None}
                self._inform_agents(agent, content)

            self.auctions[auction_id].status = 'accounted'

    def _register_at_upstream_mos(self):
        _error_trigger = 0  # is set to 1 in case of multiple upstream_mos
        for upstream_mo in self.upstream_mos:
            if _error_trigger == 1:
                # todo: register at different mos for different topics if multiple upstream mos are set
                # todo: consider todo in _handle_bid_msg()
                raise Exception('Consider todo in market operator!')

            content = create_msg_content(
                RegistrationMessage, 
                topic='market_operator'
            )

            agent = {(upstream_mo[0], upstream_mo[1]): None}
            self._inform_agents(agent, content, performative=Performatives.inform)

            _error_trigger = 1

    def _handle_registration_msg(self, content, meta):
        """Stores the agent id and topic attributes of MPAgents in order to send them gate openings
        :param content: Message content of registration message
        :param meta: All meta information in a dict.
        """
        # mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: Content: {}, current time: {}'.format(content, self.current_time))
        # TODO: folgendes muss noch mehr Registrierungen bearbeiten können
        if content.topic == 'balancing_reserve':
            self._register_mpagent(content, meta)
        elif content.topic == 'market_operator':
            self._register_moagent(content, meta)

    def _register_moagent(self, content, meta: Dict[str, Any]):
        """Stores the agent id and topic attribute 'all' of downstream MOAgents in order to send 
        all gate openings to them
        :param content: Message content of registration message
        :param meta: All meta information in a dict.
        """
        registrator = (meta['sender_addr'], meta['sender_id'])
        self.downstream_mos[registrator] = ['all']

    def _register_mpagent(self, content, meta: Dict[str, Any]):
        """Stores the agent id and topic attributes of MPAgents in order to send them gate openings
        :param content: Message content of registration message
        :param meta: All meta information in a dict.
        """
        registrator = (meta['sender_addr'], meta['sender_id'])
        if registrator in self.mp_agents:
            topics = self.mp_agents[registrator]
        else:
            topics = []
        topics.append(content.topic)
        self.mp_agents[registrator] = topics


if __name__ == '__main__':
    main()
