import asyncio
import random
from dataclasses import asdict
from typing import Tuple, Dict, Any

from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives

from mamoem.mas.mangomosaik_agent import MaMoAgent
from mamoem.mas.agent_messages import *
from mamoem.mas.market.market_util import *
from mamoem.mas.market.flexibility import FlexibilityDisposer
from mamoem.util.logger import setup_logger

mosaik_logger = setup_logger('mas.mp  ', 'logs/market.log')


class MpAgent(MaMoAgent):
    """
    A MpAgent represents a Market Participant and orchestrates message 
    handling, flexibility calculation and bidding on electricity markets.
    """

    def __init__(self, container: Container, mo_addr: (str, int), mo_id: str,
                 observer_addr=None, observer_id=None, seed=0,
                 p_max_grid_set_kw=None, cap_kwh=None, p_charge_max_kw=None, 
                 p_discharge_max_kw=None, eta_ch=None, eta_dis=None):
        super().__init__(container=container, observer_addr=observer_addr,
                         observer_id=observer_id, seed=seed)
        self.mo_addr = mo_addr
        self.mo_id = mo_id

        self.market_manager = MarketManager(
            label=self.aid,
            cost=self.random_generator.random()
        )
        self.bid_counter = 0
        self.flex_disposer = FlexibilityDisposer(
            label=self.aid,
            p_max_grid_set_kw=p_max_grid_set_kw, 
            cap_kwh=cap_kwh,
            p_charge_max_kw=p_charge_max_kw, 
            p_discharge_max_kw=p_discharge_max_kw,
            eta_ch=eta_ch, 
            eta_dis=eta_dis
        )

        # variables to be saved in database
        self.awarded_auction_id: str
        self.amount_mw: int = 8
        self.price: int = 0
        # variables saved in suitable data structure for AuctionInformationMessage
        self.auction_information = {
            'auction_id': None,
            'tender_amount': None,
            'supply_start': None,
            'awarded_amount': self.amount_mw,
            'awarded_price': self.price,
            'bids': [],
        }

    @classmethod
    async def create(
        cls, container: Container, mo_addr: (str, int), mo_id: str, 
        observer_addr: (str, int), observer_id: str, seed: float, 
        p_max_grid_set_kw, cap_kwh, p_charge_max_kw, p_discharge_max_kw,
        eta_ch, eta_dis):
        """Return a new :class:`MpAgent` instance.

        *main_container* is the main_container that the agent lives in.

        """
        # We use a factory function here because __init__ cannot be a coroutine
        # and we want to make sure the agent is registered at the controller and that
        # when we return the instance we have have a fully initialized
        # instance.
        #
        # Classmethods don't receive an instance "self" but the class object as
        # an argument, hence the argument name "cls".

        mp = cls(
            container=container, mo_addr=mo_addr, mo_id=mo_id, 
            observer_addr=observer_addr, observer_id=observer_id, 
            seed=seed, p_max_grid_set_kw=p_max_grid_set_kw, cap_kwh=cap_kwh,
            p_charge_max_kw=p_charge_max_kw, p_discharge_max_kw=p_discharge_max_kw,
            eta_ch=eta_ch, eta_dis=eta_dis
        )
        return mp

    # def handle_msg(self, content, meta: Dict[str, Any]):
    #     """Handling all incoming messages of the agent, 
    #     check time, (eventually put into message_queue,)
    #     call parent functions (for msg reading and counting).
    #     :param content: The message content
    #     :param meta: All meta information in a dict.
    #     """
    #     content, use_msg = self._begin_msg_handling(
    #         content=content, meta=meta)
    #     if use_msg: # use msg
    #         self._perform_msg_handling(content, meta)

    #         # work on earlier messages
    #         for _ in range(self.message_queue.qsize()):
    #             (content, meta) = self.message_queue.get()
    #             mosaik_logger.debug(f'From message queue: {content}, {meta}')
    #             if self._msg_time_from_present_or_past(content, meta):
    #                 self._perform_msg_handling(content, meta)
    #             else:
    #                 self.message_queue.put((content, meta))

    #         self._end_msg_handling()

    #     else: # store msg in message queue
    #         self.message_queue.put((content, meta))
            
    def _perform_msg_handling(self, content, meta):
        """Perform handling of messages, which means 
        activate specific message handling.
        :param content: The message content
        :param meta: All meta information in a dict.
        """
        if isinstance(content, TimeMessage):
            # print(content)

            if content.time_information is None:
                # register at grid operator
                self._register_at_mo()
            else:  # normal operation
                self._read_inputs(content)
                self._perform_regular_tasks()

        if isinstance(content, OpeningAuctionMessage):
            self.handle_opn_auct_msg(content, meta)

        if isinstance(content, AwardedAmountMessage):
            self.handle_awarded_amount_msg(content, meta)

        if isinstance(content, RequestInformationMessage):
            self.handle_request_information_msg(content, meta)

        if isinstance(content, ActivationMessage):
            self.handle_activation_msg(content, meta)
            
    
    def _read_inputs(self, content):
        # TODO: reset der variablen vornehmen
        self.current_time = content.time_information
        self.flex_disposer.update_soc(soc_percent_dict=content.soc_percent)
        # generate updated forecast
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Received forecast: {content.p_mw_forecast} MW')
        self.flex_disposer.update_forecast(forecast_dict=content.p_mw_forecast)

    def _perform_regular_tasks(self):
        self.market_manager.update_auctions(self.current_time)
        self.market_manager.update_bids(self.current_time)

        # bidding
        demand_updated_flexibility = True
        while demand_updated_flexibility:

            mpos = self.market_manager.get_assembled_mpo_list(
                self.flex_disposer.get_forecast_horizon_index()
            )
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Received current mpos: {mpos}')
            # print(f'MPOs: {mpos}')
            flex_all_batteries = self.flex_disposer.update_flexibility(
                mpos=mpos, current_time=self.current_time)
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Received new flexibility: {flex_all_batteries}')
            # pass only flex of one battery
            bat = list(flex_all_batteries)[0]
            flex = flex_all_batteries[bat].flex_with_mpo
            new_bids, demand_updated_flexibility = \
                self.market_manager.appoint_bids(flex=flex)
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Place new bids: {new_bids}')
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'All currently placed bids: {self.market_manager.bids}')
            mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'All current obligations: {self.market_manager.obligations}')
            
            ### simplyfied bidding
            simplyfied_bidding = False
            if simplyfied_bidding:
                self.place_bid(None)
            else:
                for bid in new_bids:
                    self.place_bid(bid)
                
        # set first simple setpoint
        mpos = self.market_manager.get_assembled_mpo_list(
            self.flex_disposer.get_forecast_horizon_index()
        )
        ##### for activation - start #####
        # TODO: erste Obligation ausklammern, bis Amplify auch mit Kapazitäts-MPOs funktioniert
        # TODO: mehrschrittig Sollwerte setzen
        mpos[0] = 0
        ##### for activation - end   #####
        self.output_setpoints = self.flex_disposer.determine_setpoints(
            mpos=mpos, current_time=self.current_time
        )
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Initial setpoint: {self.output_setpoints} '
                f'for time: {self.current_time}')

    def handle_opn_auct_msg(self, content, meta: Dict[str, Any]):
        """
        Hand a new auction to market_manager.
        :param content: The message content
        :param meta: All meta information in a dict. -- currently unused
        """
        # store open auction in MarketManager
        self.market_manager.store_new_auction(
            auction_id=content.auction_id,
            product_type=content.product_type,
            supply_start=content.supply_start,
            supply_duration=content.supply_duration,
            tender_amount=content.tender_amount,
            gate_opening_time=content.gate_opening_time,
            gate_closure_time=content.gate_closure_time,
            pricing_rule=content.pricing_rule
        )

    def place_bid(self, bid):
        # ### only for simplyfied bidding
        # content = create_msg_content(
        #     BidMessage,
        #     time_at_sender=self.current_time,
        #     auction_id=self.market_manager.get_last_place_bid_here(),
        #     bid_id=self.aid+'__'+str(self.bid_counter),
        #     amount_mw=0.1, 
        #     price=self.random_generator.random()
        # )
        # self.bid_counter += 1
        # self.schedule_instant_task(
        #     coroutine=self._container.send_message(
        #         receiver_addr=self.mo_addr,
        #         receiver_id=self.mo_id,
        #         content=content,
        #         acl_metadata={
        #             'performative': Performatives.propose, 
        #             'conversation_id': 'test',
        #             'sender_id': self.aid
        #         },
        #         create_acl=True
        #     )
        # )
        # self.msg_enumerator.increase_sent()
        # ### end simplyfied bidding
        content = create_msg_content(
            BidMessage,
            time_at_sender=self.current_time,
            auction_id=bid.auction_id,
            bid_id=self.aid+'__'+str(self.bid_counter),
            amount_mw=bid.amount_mw, 
            price=bid.price
        )
        self.bid_counter += 1
        self.schedule_instant_task(
            coroutine=self._container.send_message(
                receiver_addr=self.mo_addr,
                receiver_id=self.mo_id,
                content=content,
                acl_metadata={
                    'performative': Performatives.propose, 
                    'conversation_id': 'test',
                    'sender_id': self.aid
                },
                create_acl=True
            )
        )
        self.msg_enumerator.increase_sent()

    def handle_awarded_amount_msg(self, content, meta):
        """
        Inform via log about won auction and amount
        :param content: The message content
        :param meta: Meta information
        """
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Won auction with id: {content.auction_id}, '
            f'amount: {content.amount_mw} and price: {content.price}')
        self.awarded_auction_id = content.auction_id
        self.amount_mw = content.amount_mw
        self.price = content.price
        # ### "if" only for simplyfied bidding
        # if content.auction_id in self.market_manager.bids:
        self.market_manager.add_obligation(
            auction_id=content.auction_id, amount_mw=content.amount_mw)

    def handle_activation_msg(self, content, meta):
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Received activation: {content}')
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
            f'Activate obligation {content.auction_id} from {self.market_manager.obligations}')
        for bat in self.output_setpoints:
            self.output_setpoints[bat] = content.amount_mw * 1e6
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                f'Activation setpoint: {self.output_setpoints} '
                f'for time: {self.current_time}')

    def handle_request_information_msg(self, content, meta: Dict[str, any]):
        
        # get information from the meta dict
        receiver_addr = meta.get('sender_addr', None)
        receiver_id = meta.get('sender_id', None)
        conversation_id = meta.get('conversation_id', None)

        # create data for get_data in mosaik
        setpoints = list(self.output_setpoints.values())
        # print(setpoints)
        agent_data = {}
        # print(setpoints)
        for ri in content.requested_information:
            agent_data[ri] = setpoints[0]/1e3

        if receiver_addr is not None and receiver_id is not None:
            msg_content = create_msg_content(
                AuctionInformationMessage,
                auction_id_type=None,
                auction_id_tender_amount_supply_start=None,
                auction_id_bids=None,
                agent_data=agent_data
            )
            self.schedule_instant_task(
                self._container.send_message(
                    content=msg_content,
                    receiver_addr=receiver_addr, receiver_id=receiver_id,
                    create_acl=True, acl_metadata={
                        'sender_id': self.aid,
                        'conversation_id': conversation_id
                    }
                )
            )
            # increase messages sent for termination detection
            self.msg_enumerator.increase_sent()

    def _register_at_mo(self):
        mosaik_logger.debug(f'Agent {(self._container.addr, self.aid)}: '
                    f'Register at mo: {self.mo_addr}{self.mo_id}')
        content = create_msg_content(
            RegistrationMessage, 
            topic='balancing_reserve'
        )

        self.schedule_instant_task(
            self._container.send_message(
                receiver_addr=self.mo_addr,
                receiver_id=self.mo_id,
                content=content,
                acl_metadata={'performative': Performatives.inform, 'conversation_id': 'test',
                                'sender_id': self.aid},
                create_acl=True,
            )
        )
        self.msg_enumerator.increase_sent()

if __name__ == '__main__':
    main()
