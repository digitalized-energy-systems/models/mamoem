"""
Gateway between mosaik and the MAS.

The class :class:`MosaikAPI` implements the `low-level mosaik API`_.

The MosaikAPI also manages the root main_container for the MAS.  It starts
a :class:`MosaikAgent` and a :class:`ObserverAgent` agent within that main_container.
The MosaikAgent serves as a gateway between the Mo/Mp agents
and mosaik.

The Mo/Mp agents do not run within the root main_container but in a separate 
containers in sub processes. These subprocesses are as well managed by the MosaikAPI.

The entry point for the MAS is the function :func:`main()`.  It parses the
command line arguments and starts the :func:`run()` coroutine which runs until
mosaik sends a *stop* message to the MAS.

.. _mosaik API:
   https://mosaik.readthedocs.org/en/latest/mosaik-api/low-level.html

"""

import asyncio
import json
import time
from datetime import datetime
from typing import Any, Dict
import random

import mosaik_api

from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives

from mamoem.mas.agent_messages import *
from mamoem.mas.mangomosaik_agent import MaMoAgent
from mamoem.mas.nature import Nature
from mamoem.mas.market.mo import MoAgent
from mamoem.mas.market.mp import MpAgent
from mamoem.mas.observer.observer import ObserverAgent
from mamoem.util.logger import setup_logger
from mamoem.mas.observer.termination import MessageCounter

setup_logger('mas', 'logs/mas.log')
mosaik_logger = setup_logger('mas.mosaik', 'logs/mas.log')
market_logger = setup_logger('mas.market', 'logs/market.log')


def main():
    """Run the multi-agent system."""
    # logging.basicConfig(filename="logs/test.log", level=logging.DEBUG, format='%(levelname)s:%(message)s')
    mosaik_logger.info('Current time: {}'.format(time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())))
    return mosaik_api.start_simulation(MosaikAPI())


# The simulator meta data that we return in "init()":
META = {
    'api_version': '3.0',
    'type': 'time-based',
    'models': {
        'MoAgent': {
            'public': True,
            'params': ['mo_eid', 'upstream_mos'],
            'attrs': [
                'auction_id_type',
                'auction_id_tender_amount_supply_start',
                'auction_id_bids',
            ],
        },
        'MpAgent': {
            'public': True,
            'params': [
                'mo_addr', 
                'mo_eid',
                'p_max_grid_set_kw',
                'cap_kwh',
                'p_charge_max_kw',
                'p_discharge_max_kw',
                'eta_ch',
                'eta_dis'
            ],
            'attrs': [
                'p_set_kw', # set power to DER
                'set_percent', # set percent to DER
                'soc_percent', # soc of battery from DER
                'p_mw_forecast', # power from load
            ]
        },
    },
}


class MosaikAPI(mosaik_api.Simulator):
    """
    Interface to mosaik.
    """

    def __init__(self):
        # We need to pass the META data to the parent class which will extend
        # it and store it as "self.meta":
        super().__init__(META)
        # We have a step size of 15 minutes specified in seconds:
        self.step_size = 60 * 15  # seconds
        self.host = None
        self.port = None

        # This future will be triggered when mosaik calls "stop()":
        self.stopped = asyncio.Future()

        # Set by "run()":
        self.mosaik = None  # Proxy object for mosaik

        self.loop = None

        # Set in "init()"
        self.sid = None  # Mosaik simulator IDs

        self.main_container = None  # Root agent main_container
        self.agent_container = None  # Container for agents
        self.mosaik_agent = None
        self.mp_agents = {}
        self.observer_agent = None
        self.start_date = None
        self.container_procs = []  # List of "(proc, container_proxy)" tuples

        # Set/updated in "setup_done()"
        self.uids = {}  # agent_id: unit_id

    def init(self, sid, time_resolution=1., **sim_params):
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.init_as_coro(sid, time_resolution, **sim_params))
        #
        return META

    async def init_as_coro(self, sid, time_resolution, mas_config):
        """Create a local agent main_container and the mosaik agent."""
        self.sid = sid
        # print('MAS config: ', mas_config)
        mosaik_logger.debug('MAS config: {}'.format(mas_config))
        self.host = mas_config['host_mosaik']
        self.port = mas_config['port_mosaik']
        self.start_date = datetime.strptime(mas_config['start_date'], mas_config['date_format'])
        self.random_generator = random.Random(mas_config['seed'])
        # print(f"{mas_config['seed']}: {self.random_generator.random()}")

        # Root main_container for the MosaikAgent and Controller.
        # run until complete the best way here?
        self.main_container = await Container.factory(addr=(self.host, self.port))
        mosaik_logger.debug('Main container created')

        # Container for the MpAgents.
        self.agent_container = await Container.factory(addr=(mas_config['host_mango'], mas_config['port_mango']))
        mosaik_logger.debug('Agent container created')

        # Observer for termination detection
        self.observer_agent = ObserverAgent(self.main_container)
        mosaik_logger.info('Observer created')

        # Start the MosaikAgent agent in the root main_container:
        self.mosaik_agent = MosaikAgent(container=self.main_container,
                                        observer_addr=self.observer_agent._container.addr,
                                        observer_id=self.observer_agent.aid,
                                        seed=self.random_generator.random()
                                        )
        mosaik_logger.info('MosaikAgent created')

    def create(self, num, model, **model_conf):
        entities = self.loop.run_until_complete(self.create_as_coro(num, model, **model_conf))
        return entities

    async def create_as_coro(self, num, model, **model_conf):
        """Create *num* instances of *model* and return a list of entity dicts
        to mosaik."""
        assert model in META['models']
        entities = []

        mosaik_logger.debug('Model config: {}'.format(model_conf))
        # Get the number of agents created so far and count from this number
        # when creating new entity IDs:
        n_agents = len(self.mosaik_agent.agents)
        container = self.agent_container
        observer = self.observer_agent

        for i in range(n_agents, n_agents + num):
            if model == 'MoAgent':
                # Entity data
                if 'mo_eid' in model_conf.keys():  # if eid defined
                    eid = model_conf['mo_eid']
                else:
                    eid = 'MarketOperator_%s' % i  # else give new eid

                # if upstream mos are set, use in initialization
                upstream_mos = {}
                self.mosaik_agent.market_operator_graph[eid] = {}
                if 'upstream_mos' in model_conf.keys():
                    # print(upstream_mos)
                    for upstream_mo in model_conf['upstream_mos']:
                        assert upstream_mo in self.mosaik_agent.agents
                        mo_addr = self.mosaik_agent.agents[upstream_mo][0]
                        mo_id = self.mosaik_agent.agents[upstream_mo][1]
                        upstream_mos[(mo_addr, mo_id)] = model_conf['upstream_mos'][upstream_mo]
                        self.mosaik_agent.market_operator_graph[eid] = model_conf['upstream_mos']
                    
                agent = await MoAgent.create(container=container,
                                             upstream_mos=upstream_mos,
                                             observer_addr=observer._container.addr, 
                                             observer_id=observer.aid,
                                             seed=self.random_generator.random())
                self.mosaik_agent.market_operators[eid] = (container.addr, agent.aid)
            elif model == 'MpAgent':
                # Entity data
                eid = 'MarketParticipant_%s' % i
                mosaik_logger.debug('Model is MpAgent, Model conf: {}'.format(model_conf))

                agent = await MpAgent.create(
                    container=container, 
                    mo_addr=model_conf['mo_addr'], 
                    mo_id=self.mosaik_agent.agents[model_conf['mo_eid']][1],
                    observer_addr=observer._container.addr, 
                    observer_id=observer.aid,
                    seed=self.random_generator.random(),
                    p_max_grid_set_kw=model_conf['p_max_grid_set_kw'],
                    cap_kwh=model_conf['cap_kwh'],
                    p_charge_max_kw=model_conf['p_charge_max_kw'],
                    p_discharge_max_kw=model_conf['p_discharge_max_kw'],
                    eta_ch=model_conf['eta_ch'],
                    eta_dis=model_conf['eta_dis']
                )
            else:
                # Entity data
                eid = 'Agent_%s' % i
                agent = await MpAgent.create(container=container)
            entities.append({'eid': eid, 'type': model})

            # Store details in agents dict
            self.mosaik_agent.agents[eid] = (container.addr, agent.aid)
        # print('Entities: ', entities)
        mosaik_logger.debug('Entities: {}'.format(entities))
        # print('Mosaik agents: ', self.mosaik_agent.agents)
        mosaik_logger.debug('Mosaik Agents: {}'.format(self.mosaik_agent.agents))
        return entities

    def setup_done(self):
        """
        Get the entities that our agents are connected to once the scenario
        setup is done.
        Trigger intialization of observer agent.
        Trigger registration of all
        mo agents at upstream mo and of all and mp agents at connected mo.
        """
        full_ids = ['%s.%s' % (self.sid, eid) for eid in self.mosaik_agent.agents.keys()]

        num_agents = len(self.mosaik_agent.agents.keys()) + 1  # agents plus mosaik_agent himself
        self.observer_agent.setup_done(num_agents=num_agents,
                                       mosaik_addr=self.mosaik_agent._container.addr,
                                       mosaik_id=self.mosaik_agent.aid
                                       )

        data = {}
        for key, value in self.mosaik_agent.agents.items():
            # warning: structure of data dict not as in step method of mosaik agent
            data[key] = None

        self.loop.run_until_complete(self.mosaik_agent.step(inputs=data))

        # print('setup done... done')
        market_logger.debug(f'Market Operator Graph: {self.mosaik_agent.market_operator_graph}')
        mosaik_logger.info('Setup done... done')

    def finalize(self):
        """

        :return:
        """
        # TODO
        # await self.controller.shutdown()
        # await self.mosaik_agent.shutdown()
        # await self.main_container.shutdown()
        # await self.agent_container.shutdown()
        pass

    def step(self, time, inputs, max_advance):
        """Send the inputs of the controlled units to our agents and get new
        set-points for these units from the agents.

        This method will run for at most "step_size" seconds, even if the
        agents need longer to do their calculations.  They will then continue
        to do stuff in the background, while this method returns and allows
        mosaik to continue the simulation.

        """
        market_logger.debug("Current simulation time is: {}".format(time))
        # addr, aid = self.mp_agents['Agent_0']
        # print(datetime.fromtimestamp(self.start_date.timestamp()+time))
        # print(time)
        # print(type(time))

        mosaik_logger.debug(f'Step method in mosaik.py, what`s in inputs? {inputs}')

        for key, value in self.mosaik_agent.agents.items():
            if key not in inputs.keys():
                inputs[key] = {}
            inputs[key]['time'] = time

        self.loop.run_until_complete(self.mosaik_agent.step(time=time, inputs=inputs))

        return time + self.step_size

    def get_data(self, outputs):
        """Return the data requested by mosaik.  *outputs* is a dict like::

            {
                'wecs-0': ['v', 'P'],
                ...
            }

        Return a dict with the requested data::

            {
                'wecs-0': {
                    'v': 23,
                    'P': 42,
                },
                ...
            }
             {
                'mo-0': {
                    'name': 'test‘,
                },
                ...
            }
        """
        data = self.loop.run_until_complete(self.mosaik_agent.gather_auction_data(outputs))
        # print("This is the data in get_data: ", data)
        return data


class MosaikAgent(MaMoAgent):
    """This agent is a gateway between the mosaik API and the Mo/Mp agents.

    It forwards the current inputs to the agents, contains Nature for splitting market,
    is in contacts with the termination detection
    and then collects new set-points from the simulated agents.
    """

    def __init__(self, container: Container, observer_addr=None, observer_id=None, seed: float=None):
        super().__init__(container, observer_addr, observer_id)
        self.agents = {}  # Dict mapping mosaik eid to address (agent_addr, agent_id) (set in mosaik api)
        self.market_operators = {}  # Dict mapping mosaik eid to address (mo_addr, mo_id) (set in mosaik api)
        # self._p_max = {}          # Dict mapping from mosaik_aid to Future that is pending until a p_max reply arrives

        self.market_operator_graph = {}  # TODO: include topics

        # Dict mapping from mosaik_aid to Future. We need this to make sure all agents have received their updates.
        self._updates_received = {}

        # Done if the controller has performed one cycle
        self._agents_terminated = asyncio.Future()  # None #

        # RequestInformationMessage for bid and auction information from mo and mp
        self.auction_information_request: Dict[str, asyncio.Future] = {}
        self.mo_auction_information = {}
        self.mp_auction_information = {}
        self.agent_data = {}

        # Nature to split system regularly
        self.nature = Nature(seed=seed, res=900)

    async def step(self, inputs, time=None):
        """
        This will be called from the mosaik api once per step.

        :param inputs: the input dict from mosaik: {eid_1: {'P': p_value}, eid_2: {'P': p_value}...}
        :return: the output dict: {eid_1: p_max_value, eid_2: p_max_value}
        """

        # # 1. Evaluate grid state
        if time is not None:
            self.current_time = time
            # add connected or separated system state to inputs
            market_connections = \
                self.nature.evaluate_market_connections(
                    time, 
                    self.market_operator_graph
                )
            required_control_share = self.nature.determine_control_share(
                self.market_operators,
                market_connections
            )
            market_logger.debug(f'{time}: Required control share: {required_control_share}')
            print(f'{time}: Required control share: {required_control_share}')
            inputs = self._append_connections_and_control(
                inputs=inputs, 
                market_connections=market_connections,
                required_control_share=required_control_share
            )

        # # 2a. prepare new termination detection
        self.reset()
        # # 2b. update state of market operator agents
        await self.update_mo_agents(inputs)
        # # # 2c. wait for termination
        # await asyncio.wait_for(self._agents_terminated, timeout=3)
        # mosaik_logger.debug('Mo agents terminated.')

        # # # 3a. prepare new termination detection
        # self.reset()
        # # 3b. update state of market operator agents
        await self.update_mp_agents(inputs)
        # # 3c. wait for termination
        await asyncio.wait_for(self._agents_terminated, timeout=3)
        mosaik_logger.debug('Agents terminated.')

    def handle_msg(self, content, meta):
        """
        We expect two types of messages:
        1) AuctionInfirmationMessage with
           a) data from Mo agents about cleared auctions
           b) set points from Mp agents for DER power
        2) Termination notification from observer agent

        :param content: Content of the message
        :param meta: Meta information
        """
        content = read_msg_content(content)
        mosaik_logger.debug(f'Mosaik agent received {content} and'
                      f' meta {meta}')

        if isinstance(content, AuctionInformationMessage):
            self.handle_auction_information_message(content, meta)

        elif isinstance(content, AgentsTerminatedMessage):
            # set future to true in order to end mosaik step.
            # print('Result of agents_terminated: ',self._agents_terminated._result)
            self._agents_terminated.set_result(True)

    def handle_auction_information_message(self, content, meta):
        """
        According to agent type (found via the data related to the receiver_id),
        data from the auction_information_message (found in parameter content)
        is saved in two attributes of the Mosaik Agent (mp/mo_auction_information).

        :param content: Content of the message (AuctionInformationMessage(auction_id etc))
        :param meta: Meta information (receiver_id, receiver_addr, conversation_id)
        """
        # identify sender of message
        self.msg_enumerator.increase_rcvd()
        receiver_id = meta.get('sender_id', None)
        agent_values = self.agents.values()
        # list comprehension method --> converts into a list of tuples
        agent_lists_tuples = [(key, value) for key, value in self.agents.items()]

        for agent in agent_lists_tuples:
            # distinguish between MarketOperator agent and MarketParticipant agent
            eid = agent[0]
            assert ('MarketOperator' in eid) or ('MarketParticipant' in eid)
            # find out id of format: "agent_i"
            agent_id = agent[1][1]
            # find out whose auction information message we received
            if receiver_id == agent_id:
                # act according to agent type
                if 'MarketOperator' in eid:
                    # serialize "bid-list"
                    bids = json.dumps(content.auction_id_bids)
                    auction_id_type = json.dumps(content.auction_id_type)
                    auction_id_tender_amount_supply_start = json.dumps(
                        content.auction_id_tender_amount_supply_start
                    )
                    self.mo_auction_information[eid] = {
                        'auction_id_type': auction_id_type,
                        'auction_id_tender_amount_supply_start': 
                            auction_id_tender_amount_supply_start,
                        'auction_id_bids': bids,
                    }
                elif 'MarketParticipant' in eid:
                    # print("agent typ MP: ", eid)
                    self.agent_data[eid] = content.agent_data
        if self.observer_addr is not None:  # only if observer is set
            self._inform_observer()
        self.msg_enumerator.reset()

    async def update_mo_agents(self, data):
        """Update the agents with new data from mosaik."""
        futs = []
        # print(data)
        for mosaik_eid, input_data in data.items():
            if mosaik_eid in self.market_operators:
                # print(f'Input: {input_data}')
                if input_data is None:
                    time_information = None
                    imbalance = None
                else:
                    time_information = input_data['time']
                    imbalance = input_data['imbalance']

                # TODO: market connections so, dass ein leeres dict verschickt wird, wenn keine weitere Verbindung nach oben?
                if input_data is None:
                    market_connections = None
                elif 'market_connections' in input_data:
                    market_connections = input_data['market_connections']
                else:
                    market_connections = None

                content = create_msg_content(
                    MoTimeMessage, 
                    time_information=time_information,
                    imbalance=imbalance
                )
                fut = self.schedule_instant_task(
                    self._container.send_message(
                        receiver_addr=self.agents[mosaik_eid][0],
                        receiver_id=self.agents[mosaik_eid][1],
                        content=content,
                        acl_metadata={
                            'performative': Performatives.inform, 
                            'conversation_id': mosaik_eid,
                            'sender_id': self.aid},
                        create_acl=True,
                    )
                )
                futs.append(fut)
        await asyncio.gather(*futs)

        self.msg_enumerator.increase_sent(len(futs)) # count outgoing messages
        if self.observer_addr is not None:  # only if observer is set
            self._inform_observer()
        self.msg_enumerator.reset()

    async def update_mp_agents(self, data):
        """Update the agents with new data from mosaik."""
        futs = []
        # print(data)
        for mosaik_eid, input_data in data.items():
            if input_data is None:
                time_information = None
            else:
                time_information = input_data['time']

            if mosaik_eid not in self.market_operators:
                if input_data is None:
                    soc_percent = None
                elif 'soc_percent' in input_data:
                    soc_percent = input_data['soc_percent']
                else:
                    soc_percent = None
                if input_data is None:
                    p_mw_forecast = None
                elif 'p_mw_forecast' in input_data:
                    p_mw_forecast = input_data['p_mw_forecast']
                else:
                    p_mw_forecast = None

                content = create_msg_content(
                    MpTimeMessage, time_information=time_information,
                    soc_percent=soc_percent,
                    p_mw_forecast=p_mw_forecast
                )
                fut = self.schedule_instant_task(
                    self._container.send_message(
                        receiver_addr=self.agents[mosaik_eid][0],
                        receiver_id=self.agents[mosaik_eid][1],
                        content=content,
                        acl_metadata={
                            'performative': Performatives.inform, 
                            'conversation_id': mosaik_eid,
                            'sender_id': self.aid},
                        create_acl=True,
                    )
                )
                futs.append(fut)
        await asyncio.gather(*futs)

        self.msg_enumerator.increase_sent(len(futs)) # count outgoing messages
        if self.observer_addr is not None:  # only if observer is set
            self._inform_observer()
        self.msg_enumerator.reset()

    async def gather_auction_data(self, outputs):
        """
        According to agent type (found via the data related to the receiver_id),
        data from the auction_information_message (found in parameter content)
        is saved in two attributes of the Mosaik Agent (mp/mo_auction_information).

        :param outputs: dict: contains all agents and the params defined in the connect_many_to_one
        command in scenario.py
        """
        # print(f'Outputs in gather_auction_data: {outputs}')
        # print(self.agents)
        cycle_no = 0
        # print("IN GATHER AUCTION DATA")
        self.msg_enumerator.reset()
        self.reset()
        # Send RequestInformationMessage for current_p to all known agents
        for eid in self.agents:
            (agent_addr, agent_id) = self.agents[eid]
            # create unique conversation_id
            conversation_id = f'{agent_addr}_{agent_id}_{cycle_no}'
            cycle_no += 1

            msg_content = create_msg_content(RequestInformationMessage, requested_information=outputs[eid])
            self.schedule_instant_task(
                coroutine=self._container.send_message(
                    content=msg_content,
                    receiver_addr=agent_addr, receiver_id=agent_id, create_acl=True,
                    acl_metadata={
                        'conversation_id': conversation_id,
                        'performative': Performatives.request,
                        'sender_id': self.aid
                    }
                )
            )
            self.msg_enumerator.increase_sent()
        if self.observer_addr is not None:  # only if observer is set
            self._inform_observer()
        self.msg_enumerator.reset()
        await asyncio.wait_for(self._agents_terminated, timeout=3)

        data = {}
        for eid, attrs in outputs.items():
            if eid not in self.agents:
                raise ValueError('Unknown entity ID "%s"' % eid)
            # print("eid: ", eid, "attrs: ", attrs)
            idx = self.agents[eid]
            data[eid] = {}
            for attr in attrs:
                # Todo: Nicht vergessen: ggfs. wieder einkommentieren:
                # Müsste es nicht auch für ['MpAgent'] geprüft werden?
                # if attr not in self.meta['models']['MoAgent']['attrs']:
                #     raise AttributeError('Attribute "%s" not available' % attr)
                data[eid][attr] = 1000
                # Todo: hier wird gerade nur nach Typ und nicht nach genauem Agenten unterschieden,
                # also ob Market_Operator_1 oder _2 etc?
                if 'MarketOperator' in eid:
                    if attr == 'auction_id_type':
                        data[eid][attr] = self.mo_auction_information[eid][attr]
                    if attr == 'auction_id_tender_amount_supply_start':
                        data[eid][attr] = self.mo_auction_information[eid][attr]  # outputs.get('auction_id')
                    if attr == 'auction_id_bids':
                        mosaik_logger.debug("IN BIDS, attr: {}, self.mo_auction_information[eid][attr]: {}".format(
                            attr, self.mo_auction_information[eid][attr]))
                        data[eid][attr] = self.mo_auction_information[eid][attr]
                if 'MarketParticipant' in eid:
                    # print(f'Return for {eid} and {attr}: {self.agent_data[eid]}')
                    data[eid][attr] = self.agent_data[eid][attr]
                    continue

        mosaik_logger.debug(f'This is the data to return: {data}')
        return data
    
    def _count_messages(self, content, meta):
        mosaik_logger.debug('In: ', content.messages_in, 'Out: ', content.messages_in)

    def _append_connections_and_control(
        self, inputs, market_connections, required_control_share):
        # market connections
        for mo in market_connections:
            relevant_connections = {}
            for connected_eid in market_connections[mo]:
                relevant_connections[str(self.agents[connected_eid])] = \
                    market_connections[mo][connected_eid]
            inputs[mo]['market_connections'] = relevant_connections
        # control share
        for mo in self.market_operators: 
            inputs[mo]['imbalance'] = required_control_share[mo]
        return inputs

    def reset(self):
        """
        :return:
        """
        # self._p_max = {aid: asyncio.Future() for aid in self.agents.keys()}
        # self._updates_received = {aid: asyncio.Future() for aid in self.agents.keys()}
        self._agents_terminated = asyncio.Future()


if __name__ == '__main__':
    main()
