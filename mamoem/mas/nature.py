import random

class Nature:

    def __init__(self, seed: float=None, res: int=15*60):
        self.random_generator = random.Random(seed)
        assert 3600 % res == 0
        self.res = res
        self.market_connections_over_time = {}

    def evaluate_market_connections(self, time: int, probability_graph: dict):
        """
        Takes a time and a graph and returns a graph of the same structure with True and
        False with a probability given in the graph. This action is performed every four
        time steps and represents, whether mos are connected to their upstream mo in the
        respective simulation time step.
        :param time: current simulation time 
        :param probability_graph: dict of dicts in which probabilies are contained
        """
        if time not in self.market_connections_over_time:
            connections_graph = {}
            # quarter_hour_count = (time % 3600) / 900
            for mo in probability_graph:
                connections_graph[mo] = {}
                for upstream_connection in probability_graph[mo]:

                    initializer = self.random_generator.random()
                    prob = probability_graph[mo][upstream_connection]
                    if initializer < prob:
                        connections_graph[mo][upstream_connection] = True
                    else:
                        connections_graph[mo][upstream_connection] = False
                # agent_graph_connections[]
            for time_index in range(int(3600/self.res)):
                self.market_connections_over_time[time+time_index*self.res] = connections_graph

        return self.market_connections_over_time[time]

    def determine_control_share(self, market_operators: dict, market_connections: dict):
        control_share = {}
        for mo in market_operators:
            if not market_connections[mo]:
                # mo without upstream always needs control
                # control_share[mo] = 2 * round(self.random_generator.random(), 2) - 1
                # for simplicity first only positive or negative imbalance
                if self.random_generator.random() < 0.5:
                    control_share[mo] = -1
                else:
                    control_share[mo] = 1
            elif not any(market_connections[mo].values()): # TODO: market_connections noch auspacken
                # mo with disconnected upstream needs control as well
                # control_share[mo] = 2 * round(self.random_generator.random(), 2) - 1
                # for simplicity first only positive or negative imbalance
                if self.random_generator.random() < 0.5:
                    control_share[mo] = -1
                else:
                    control_share[mo] = 1

            else:
                # mo with connected upstream has no need
                control_share[mo] = 0
        return control_share