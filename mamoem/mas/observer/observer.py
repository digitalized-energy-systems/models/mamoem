import asyncio
import logging

import mosaik_api

from typing import Any, Dict

from mango.core.agent import Agent
from mango.core.container import Container
from mango.messages.message import Performatives
from mamoem.mas.observer.termination import MessageCounter

from mamoem.mas.agent_messages import *
from mamoem.mas.mangomosaik_agent import MaMoAgent

from datetime import datetime

from mamoem.util.logger import setup_logger

observer_logger = setup_logger('mas.observer', 'logs/observer.log')


class ObserverAgent(Agent):
    """
    An ObserverAgent is used for termination detection of the step-method
    """

    def __init__(self, container: Container):
        super().__init__(container)
        self._message_counter = None

    def handle_msg(self, content, meta: Dict[str, Any]):
        """
        From Agent.
        Has to be implemented by the user.
        """
        content = read_msg_content(content)

        observer_logger.debug(f'{self._aid} received a message with content {content} and'
              f' meta {meta}')

        if isinstance(content, CountMessagesMessage):
            self._handle_count_messages_msg(content=content, meta=meta)

    def _handle_count_messages_msg(self, content, meta):
        """
        Receive CountMessagesMessage and trigger termination detection.
        If termination detected, send message to mosaikAgent.
        """
        # print('Oberserver input: ',(meta['sender_addr'], meta['sender_id']), 
        observer_logger.debug('Oberserver input from: {}, in: {}, out: {}'.format((meta['sender_addr'], meta['sender_id']),
            content.messages_in, content.messages_out))
        # optional Todo: make tuple from meta['sender_addr'] in order to prevent list
        host, port = meta['sender_addr']
        sender_addr = (host, port)
        termination_detected = self._message_counter.update(
            (sender_addr, meta['sender_id']), 
        # termination_detected = self._message_counter.update(
        #     (meta['sender_addr'], meta['sender_id']), 
            msgs_in=content.messages_in,
            msgs_out=content.messages_out
        )
        observer_logger.debug('End detection')
        if termination_detected:
            
            self.schedule_instant_task(
                self._container.send_message(
                    receiver_addr=self.mosaik_addr,
                    receiver_id=self.mosaik_id,
                    content=create_msg_content(AgentsTerminatedMessage),
                    acl_metadata={'performative': Performatives.inform, 'conversation_id': None,
                                'sender_id': self.aid},
                    create_acl=True,
                )
            )
            self._message_counter.reset()
        observer_logger.debug('Pause Counting')

    def setup_done(self, num_agents: int, mosaik_addr: (str,int) = None, mosaik_id: str = None):
        """
        Callback during setup_done of mosaik. Used to initialize internal MessageCounter and
        receive address of mosaikAgent.
        """
        self._message_counter = MessageCounter(num_agents)
        self._message_counter.reset()
        assert mosaik_addr is not None
        self.mosaik_addr = mosaik_addr
        assert mosaik_id is not None
        self.mosaik_id = mosaik_id
        observer_logger.debug('MessageCounter created')
