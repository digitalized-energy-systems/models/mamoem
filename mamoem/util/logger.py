import logging
import sys

formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')


def setup_logger(name, log_file, level=logging.DEBUG, propagate=True):
    """To setup as many loggers as you want. Be aware of the log-levels: debug - info - warning - error - critical
    if level is set to info, debug is not going to be written into the log_file"""

    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    if not propagate:
        logger.propagate = False
    if name == 'mas':
        logger.propagate = False

    return logger
