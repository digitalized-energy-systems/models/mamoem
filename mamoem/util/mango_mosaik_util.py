
class MessagesEnumerator:
    """
    Enumerator, which contains counters for incoming (rcvd) and outgoing (sent) messages.
    They can be increased or reset to zero (0).
    """
    def __init__(self):
        self.num_rcvd_msgs = 0
        self.num_sent_msgs = 0

    def get_rcvd(self):
        return self.num_rcvd_msgs

    def get_sent(self):
        return self.num_sent_msgs

    def increase_rcvd(self, increment=1):
        self.num_rcvd_msgs = self.num_rcvd_msgs+increment

    def increase_sent(self, increment=1):
        self.num_sent_msgs = self.num_sent_msgs+increment
    
    def reset(self):
        self.num_rcvd_msgs = 0
        self.num_sent_msgs = 0