import pandas as pd
import numpy as np

import plotly.graph_objects as go
from plotly.subplots import make_subplots

from mamoem.visual.db_extract import *
from mamoem.visual.data_evaluator import DataEvaluator
from mamoem.util.logger import setup_logger

vis_logger = setup_logger('visual', 'logs/visualisation.log')

# ## Inspiration & sources ## #

# ? added subsequently different traces to the bar chart like so:
# https://stackoverflow.com/questions/60148499/plotly-express-how-to-control-bars-start-position

# ? for color scale coloring of bars:
# https://stackoverflow.com/questions/61892036/plotly-how-to-colorcode-plotly-graph-objects-bar-chart-using-python


class AuctionVisualizer:
    def __init__(
        self,
        filepath: str,
        write_all_data_to_excel=True,
        write_mo_data_to_excel=True,
    ):

        self.filepath = filepath
        self.filepath_xlsx = Path("data/db_all_entries.xlsx")
        if os.path.exists(self.filepath_xlsx):
            os.remove(self.filepath_xlsx)
        self.xlsx_writer = pd.ExcelWriter(self.filepath_xlsx)
        self.write_all_data_to_excel = write_all_data_to_excel
        self.write_mo_data_to_excel = write_mo_data_to_excel
        self.db = DataExtraction(
            filepath=self.filepath,
            write_all_data_to_excel=self.write_all_data_to_excel,
            write_mo_data_to_excel=self.write_mo_data_to_excel,
        )

        self.auction_data = {}
        # auction_data has the following structure:
        # auction_data = {'auction_id': str,
        #     'market_operators': [mo1, mo2, ...],
        #     'tender_amount': {mo1: ta1, mo2: ta2, ...},
        #     'bids': DataFrame,
        #     'marginal_price': {...},
        #     ..., }
        self.auctions_data = {}
        self.pos_auctions_data = {}
        self.neg_auctions_data = {}
        self.multi_auctions_data = {}
        
        self.battery_data = {}
        # battery_data has the following structure:
        # battery_data = {'batteries': [bat1, bat2, ...],
        #     'socs': {bat1: [socs1], bat2: [socs2], ...},
        #     ..., }

    # ## Helper method(s) ## #

    def rgb_to_rgba(self, rgb_value, alpha):
        """
        Adds the alpha channel to an RGB Value and returns it as an RGBA Value
        :param rgb_value: Input RGB Value
        :param alpha: Alpha Value to add  in range [0,1]
        :return: RGBA Value
        """
        return f"rgba{rgb_value[3:-1]}, {alpha})"

    def set_bar_color_and_text(self, fig, trace_alphas):  # , df):
        """
        Updates the color scheme and the text for/on the bars for all traces
        """

        DEFAULT_PLOTLY_COLORS = [
            "rgb(31, 119, 180)",
            "rgb(255, 127, 14)",
            "rgb(44, 160, 44)",
            "rgb(214, 39, 40)",
            "rgb(148, 103, 189)",
            "rgb(140, 86, 75)",
            "rgb(227, 119, 194)",
            "rgb(127, 127, 127)",
            "rgb(188, 189, 34)",
            "rgb(23, 190, 207)",
        ]
        DEFAULT_PLOTLY_COLORS_RGBA = []

        for rgb in DEFAULT_PLOTLY_COLORS:
            # second paramater in rgb_to_rgba is defined like this:
            # 0.0 = fully transparent, 1.0 = fully opaque
            DEFAULT_PLOTLY_COLORS_RGBA.append(self.rgb_to_rgba(rgb, 1.0))

        num = 0
        for trace in fig.data:
            color = self.rgb_to_rgba(
                DEFAULT_PLOTLY_COLORS[num],
                trace_alphas[trace_alphas.index[num]] * 0.5 + 0.5,
            )
            # text = "Amount: " + str(df["Amount"][num])
            trace.update(
                marker_color=color,
                # all following text-parameters serve to configure text written on or on top of bars
                # text=text,
                textfont_size=14,
                # textangle=315,
                textposition="outside",
            )
            num += 1

    def set_bar_color_depending_on_share(self, fig, share):  # , df):
        """
        Updates the color to light green/blue depending on share
        for/on all traces in the given fig
        """

        trace_num = 0
        for trace in fig.data:
            rgb_string = str(
                255 * (1 - share[share.index[trace_num]] * 0.75 + 0.25)
            )
            color = self.rgb_to_rgba(f"rgb(70, {rgb_string}, 200)", 1.0)
            # text = "Amount: " + str(df["Amount"][trace_num])
            trace.update(
                marker_color=color,
            )
            trace_num += 1

    def extract_soc_data(self, periods: list):
        try:
            self.battery_data = self.db.extract_socs(periods)
        except TypeError as e:
            vis_logger.debug(
                f" db.extract_socs returned None values, there might be a problem"
                f" with the batteries or in the passed period. "
                f"\nError message: {e}"
            )
    
    def extract_p_set_kw_data(self, periods: list):
        try:
            self.battery_setpoints = self.db.extract_p_set_kw(periods)
        except TypeError as e:
            vis_logger.debug(
                f" db.extract_p_set_kw returned None values, there might be a problem"
                f" with the batteries or in the passed period. "
                f"\nError message: {e}"
            )

    def generate_soc_plots(self, periods: list):
        self.extract_soc_data(periods)
        self.extract_p_set_kw_data(periods)
        fig = go.Figure()
        # prepare x-axis
        x_df = pd.DataFrame(periods, columns=['x'])

        for battery in self.battery_data['batteries']:
            # prepare text labels
            texts = []
            mp_num = int(battery.split("__")[1]) + \
                    self.battery_setpoints['enum_offset']
            mp_key = "MarketParticipant_" + str(mp_num)
            setpoints = self.battery_setpoints['p_set_kw'][mp_key]
                
            for idx in range(len(self.battery_data['p_kw'][battery])):
                label = self.battery_data['p_kw'][battery][idx]
                texts.append(
                    f'Power: {round(label, 3)} kW<br>'
                    f'Setpoint: {round(setpoints[idx], 3)} kW'
                )
            fig.add_trace(
                go.Scatter(
                    x=x_df['x']+1, 
                    y=self.battery_data['socs'][battery],
                    mode='lines+markers',
                    name=battery,
                    text=texts
            ))
        fig.update_xaxes(
            # add axis title
            title="15-min period of time within simulation",
            tick0=0,
            dtick=1
        )
        fig.update_yaxes(
            # add axis title
            title="State of energy in %",
        )
        fig.update_layout(
            font_size=20,
        )
        return fig
        # print(self.battery_data)

    def generate_total_costs_box_plot(self, periods: list):
        multiple_periods_expenses = {}
        for period in periods:
            self.extract_and_preprocess_data(period)
            # initialize DataFrames
            if len(multiple_periods_expenses.values()) == 0:
                multiple_periods_expenses["total_expenses"] = pd.DataFrame(
                        columns=self.auction_data["expenses"].columns
                    )
                for mo in self.auction_data["market_operators"]:
                    multiple_periods_expenses[mo] = pd.DataFrame(
                        columns=self.auction_data["expenses"].columns
                    )
            
            multiple_periods_expenses["total_expenses"].loc[period] = \
                self.auction_data["expenses"].loc["total_expenses"]
            for mo in self.auction_data["market_operators"]:
                # print(self.auction_data["expenses"].loc[mo])
                multiple_periods_expenses[mo].loc[period] = \
                    self.auction_data["expenses"].loc[mo]
        print(multiple_periods_expenses)
        
        fig = make_subplots(
            rows=1,
            cols=1,
            subplot_titles=(
                "Total expenses", 
                "MarketOperator_0",
                "MarketOperator_1",
                "MarketOperator_2",
            ),
            shared_xaxes=False,
            shared_yaxes=True,
        )
        fig.update_layout(
            showlegend=False,
            font_size=20,
        )

        fig.add_trace(go.Box(y=multiple_periods_expenses["total_expenses"]["local"],
            boxpoints='all', # can also be outliers, or suspectedoutliers, or False
            jitter=0.3, # add some jitter for a better separation between points
            pointpos=-1.8, # relative position of points wrt box
            name="Local auction"
        ),
        row=1, col=1
        )

        fig.add_trace(go.Box(y=multiple_periods_expenses["total_expenses"]["liftup_liftdown"],
            boxpoints='all', # can also be outliers, or suspectedoutliers, or False
            jitter=0.3, # add some jitter for a better separation between points
            pointpos=-1.8, # relative position of points wrt box
            name="Hierarchical auction<br> with increased price"
        ),
        row=1, col=1
        )

        fig.add_trace(go.Box(y=multiple_periods_expenses["total_expenses"]["redup_liftdown"],
                boxpoints='all', # can also be outliers, or suspectedoutliers, or False
                jitter=0.3, # add some jitter for a better separation between points
                pointpos=-1.8, # relative position of points wrt box
                name="Hierarchical auction<br> with decreased price"
            ),
            row=1, col=1
            )

        fig.update_yaxes(
            range=[0, 1.1*max(multiple_periods_expenses["total_expenses"]["local"])], 
            title="Accumulated costs in EUR"
        )
        return fig

    def generate_refund_rules_box_plot(self, periods: list):
        multiple_periods_expenses = {}
        for period in periods:
            self.extract_and_preprocess_data(period)
            # initialize DataFrames
            if len(multiple_periods_expenses.values()) == 0:
                multiple_periods_expenses["total_expenses"] = pd.DataFrame(
                        columns=self.auction_data["expenses"].columns
                    )
                for mo in self.auction_data["market_operators"]:
                    multiple_periods_expenses[mo] = pd.DataFrame(
                        columns=self.auction_data["expenses"].columns
                    )
            
            multiple_periods_expenses["total_expenses"].loc[period] = \
                self.auction_data["expenses"].loc["total_expenses"]
            for mo in self.auction_data["market_operators"]:
                # print(self.auction_data["expenses"].loc[mo])
                multiple_periods_expenses[mo].loc[period] = \
                    self.auction_data["expenses"].loc[mo]
        print(multiple_periods_expenses)
        
        fig = make_subplots(
            rows=len(multiple_periods_expenses.keys()),
            cols=1,
            subplot_titles=(
                "Total expenses", 
                "MarketOperator_0",
                "MarketOperator_1",
                "MarketOperator_2",
            ),
            shared_xaxes=False,
            shared_yaxes=True,
        )
        fig.update_layout(showlegend=False)

        for col in multiple_periods_expenses[mo].columns:
            mo = "total_expenses"
            fig.add_trace(go.Box(y=multiple_periods_expenses[mo][col],
                boxpoints='all', # can also be outliers, or suspectedoutliers, or False
                jitter=0.3, # add some jitter for a better separation between points
                pointpos=-1.8, # relative position of points wrt box
                name=col
            ),
            row=1, col=1
            )
            mo = "MarketOperator_0"
            fig.add_trace(go.Box(y=multiple_periods_expenses[mo][col],
                boxpoints='all', # can also be outliers, or suspectedoutliers, or False
                jitter=0.3, # add some jitter for a better separation between points
                pointpos=-1.8, # relative position of points wrt box
                name=col
            ),
            row=2, col=1
            )
            mo = "MarketOperator_1"
            fig.add_trace(go.Box(y=multiple_periods_expenses[mo][col],
                boxpoints='all', # can also be outliers, or suspectedoutliers, or False
                jitter=0.3, # add some jitter for a better separation between points
                pointpos=-1.8, # relative position of points wrt box
                name=col
            ),
            row=3, col=1
            )
            mo = "MarketOperator_2"
            fig.add_trace(go.Box(y=multiple_periods_expenses[mo][col],
                boxpoints='all', # can also be outliers, or suspectedoutliers, or False
                jitter=0.3, # add some jitter for a better separation between points
                pointpos=-1.8, # relative position of points wrt box
                name=col
            ),
            row=4, col=1
            )

        return fig

    def generate_merit_order_list(self, periods: list):
        """Helper method to calculate merit orders of multiple time steps."""
        figs = []
        for period in periods:
            fig = self.generate_merit_order_for(period)
            figs.append(fig)
        return figs

    def generate_merit_order_for(self, period: int):
        """Get auction data from database, perform some calculation on the
        data and extract data of single market operators for visualisation.

        Parameters
        ----------
        period: the time interval's index, for which an auction is visualised
        """
        self.extract_and_preprocess_data(period=period)

        return self._make_multiple_merit_order()

    def generate_merit_order_table_list(self, periods: list):
        """Helper method to calculate merit orders of multiple time steps."""
        figs = []
        for period in periods:
            fig = self.generate_merit_order_table_for(period)
            figs.append(fig)
        return figs

    def generate_merit_order_table_for(self, period: int):
        """Get auction data from database, perform some calculation on the
        data and extract data of single market operators for visualisation.

        Parameters
        ----------
        period: the time interval's index, for which an auction is visualised
        """
        self.extract_and_preprocess_data(period=period)

        return self._make_multiple_merit_order_table()

    def extract_and_preprocess_multiple_auctions(self, period: int):
        
        multi_auction_data = self.db.extract_multiple_auctions(period)
        
        # print(multi_auction_data["negative_control_reserve"])

        return [
            self.preprocess_multiple_auctions(
                auction_data=multi_auction_data[
                    "positive_control_reserve"
                ]
            ),
            self.preprocess_multiple_auctions(
                auction_data=multi_auction_data[
                    "negative_control_reserve"
                ]
            )
        ]

    def extract_and_preprocess_data(self, period: int):
        # extract from database
        try:
            self.auction_data = self.db.extract_auction(period)
        except TypeError as e:
            vis_logger.debug(
                f" db_extract returned None values, there might not have been any bids in this period. "
                f"\nError message: {e}"
            )
        print(self.auction_data)
        # preprocess
        self.auction_data = self.preprocess_multiple_auctions(
            auction_data=self.auction_data)

    def preprocess_multiple_auctions(self, auction_data):
        # for each bid count number of receiving mos
        auction_data = DataEvaluator.calculate_receiving_mo_number(
            auction_data
        )
        # for each mo individually determine marginal price
        auction_data = DataEvaluator.calculate_marginal_price(
            auction_data
        )
        # print(auction_data)
        # maximum awarded amount of each bid over all mos
        auction_data = DataEvaluator.calculate_max_awarded_amount(
            auction_data
        )
        # tender amount multiplied with marginal price
        auction_data = DataEvaluator.calculate_mo_expenses(
            auction_data, calculate_scenarios=True
        )
        # print(auction_data)
        return auction_data

    def _make_multiple_merit_order(self):
        # build the figure
        num_market_operators = len(self.auction_data["market_operators"])
        fig = make_subplots(
            rows=num_market_operators,
            cols=1,
            shared_xaxes=False,
            vertical_spacing=0.15,
            specs=[[{"type": "bar"}]] * num_market_operators,
        )
        # draw merit order plots
        for mo in self.auction_data["market_operators"]:
            moidx = self.auction_data["market_operators"].index(mo)
            fig = self._draw_merit_order(
                fig,
                df_new_index=self.auction_data["bids"].loc[
                    self.auction_data["bids"]["MarketOperator"] == mo
                ],
                uniform_price=self.auction_data["marginal_price"].loc[mo, "connected"],
                tender_amount=self.auction_data["tender_amount"][mo],
                market_operator=self.auction_data["market_operators"][moidx],
                row=moidx + 1,
            )
        # fig.layout.xaxis.title=None
        # fig.layout.xaxis2.title=None
        return fig

    def _make_multiple_merit_order_table(self):
        # build the figure
        num_market_operators = len(self.auction_data["market_operators"])
        fig = make_subplots(
            rows=num_market_operators,
            cols=2,
            subplot_titles=("Merit order", "Annotations"),
            shared_xaxes=False,
            column_widths=[2, 1],
            horizontal_spacing=0.03,
            specs=[[{"type": "bar"}, {"type": "table"}]]
            * num_market_operators,
        )
        # draw merit order plots
        for mo in self.auction_data["market_operators"]:
            moidx = self.auction_data["market_operators"].index(mo)
            fig = self._draw_merit_order(
                fig,
                df_new_index=self.auction_data["bids"].loc[
                    self.auction_data["bids"]["MarketOperator"] == mo
                ],
                uniform_price=self.auction_data["marginal_price"].loc[mo, "connected"],
                tender_amount=self.auction_data["tender_amount"][mo],
                market_operator=self.auction_data["market_operators"][moidx],
                row=moidx + 1,
            )
        # draw tables
        # draw all plots before, otherwise conflicts within plotly can arise
        for mo in self.auction_data["market_operators"]:
            moidx = self.auction_data["market_operators"].index(mo)
            fig = self._draw_table(
                fig,
                market_operator=self.auction_data["market_operators"][moidx],
                row=moidx + 1,
            )

        return fig

    def _make_merit_order(
        self, df_new_index, uniform_price, tender_amount, market_operator
    ):
        """This script is used to visualize data from the database.
        It takes extracted data as a dataframe and visualizes it using plotly
        graph objects.
        """
        # print(f"DataFrame:\n{df_new_index}")
        # build the figure
        fig = make_subplots(
            rows=1,
            cols=2,
            subplot_titles=("Merit order", "MO expenses"),
            shared_xaxes=False,
            column_widths=[2, 1],
            horizontal_spacing=0.03,
            vertical_spacing=0.00,
            specs=[[{"type": "bar"}, {"type": "table"}]],
        )
        return self._draw_merit_order(
            fig, df_new_index, uniform_price, tender_amount, market_operator
        )

    def _draw_merit_order(
        self,
        fig,
        df_new_index,
        uniform_price,
        tender_amount,
        market_operator,
        row=1,
    ):
        for agent in range(0, len(df_new_index.index)):
            # add_trace needs single datapoints that's why we need to slice the dataframe one by one
            df_single_agent = df_new_index[agent : agent + 1]
            # print("this is the df_single_agent", df_single_agent)
            rgb_string = str(
                255
                * (
                    1
                    - df_new_index["Max_awarded_share"].iloc[agent] * 0.75
                    + 0.25
                )
            )
            color = self.rgb_to_rgba(f"rgb(70, {rgb_string}, 200)", 1.0)

            fig.add_trace(
                go.Bar(
                    x=df_single_agent["Cumulative_capacity_MW_connected"],
                    y=df_single_agent["Price_EUR"],
                    showlegend=False,
                    # we need the offset to relocate the bars
                    # it doesn't make much sense to offset them by the whole "Amount"-value,
                    # but that's the way it works. Feel free to play around and see for yourself.
                    offset=-df_single_agent["Amount_MW"],
                    # sets the width of the bar so that it is correctly representing the bidded amount
                    width=df_single_agent["Amount_MW"],
                    # modify information showed on hovering bars. reads <html> tags. <br> = break line,
                    # <extra></extra> at the end is used to deactivate the visualization of "trace_i" when hovering
                    # which is just a bit more ugly
                    hovertemplate="Bid id: {}<br>"
                    "Amount (MW): {} <br>"
                    "Price (€/MW): {} <br>"
                    "Cumulative amount (MW): {} <br>"
                    "Awarded share: {} <br>"
                    "<extra></extra>"
                    # values[0] because we refer to the first entry in the
                    # column, otherwise we would have meta-information
                    # included (e.g. "Name: Amount, dtype: int64"):
                    .format(
                        df_single_agent["Bid_id"].values[0],
                        df_single_agent["Amount_MW"].values[0],
                        df_single_agent["Price_EUR"].values[0],
                        df_single_agent["Cumulative_capacity_MW_connected"].values[0],
                        round(df_single_agent["Awarded_share"].values[0], 3),
                    ),
                    marker_color=color,
                ),
                row=row,
                col=1,
            )
        # coloring the traces via set_color_scheme
        # self.set_bar_color_depending_on_share(
        #     fig, df_new_index["Max_awarded_share"])

        # adding vertical line (tender amount)
        # * vline doesn't have the option to limit the line,
        # * see (https://plotly.com/python-api-reference/generated/plotly.graph_objects.Figure.html)
        # * if it is wanted to limit the lines so that there is no "cross" in the graph, use add_shape and
        # * add_annotation for both lines
        fig.add_vline(
            x=tender_amount,
            annotation_text="Tender amount:<br>" + str(tender_amount) + " MW",
            annotation_position="top left",
            annotation_align="right",
            row=row,
            col=1,
        )
        # adding horizontal line (uniform price)
        fig.add_hline(
            y=uniform_price,
            annotation_text="Uniform price:<br>"
            + str(round(uniform_price, 3))
            + " €/MW",
            annotation_position="top right",
            annotation_align="right",
            row=row,
            col=1,
        )
        # format plot
        if df_new_index.shape[0] > 0:
            fig.update_xaxes(
                range=[
                    -0.1,1.5
                    # max(
                    #     0.5,
                    #     df_new_index["Cumulative_capacity_MW_connected"].iloc[
                    #         df_new_index.shape[0] - 1
                    #     ] * 4 / 3
                    # )
                ],
                # add axis title
                title="Amount in MW",
                row=row,
                col=1,
            )
        fig.update_yaxes(
            # add axis title
            title="Bid price<br>in EUR/MW",
            row=row,
            col=1,
        )
        fig.update_layout(
            font_size=20,
        )

        return fig

    def _draw_table(self, fig, market_operator, row=1):
        # add table for further information
        fig.add_trace(
            go.Table(
                columnwidth=[2, 1, 1],
                header=dict(
                    values=["Scenario", "Local value in EUR", "Total value in EUR"],
                    font=dict(size=20),
                    align="left",
                ),
                cells=dict(
                    values=[
                        [
                            # "Total expenses",
                            "Local auctions",
                            # "Total liftup_liftdown",
                            "Hierarchical auction<br>with increased price",
                            # "Total liftup_reddown",
                            # "Local liftup_reddown",
                            # "Total redup_liftdown",
                            "Hierarchical auction<br>with decreased price",
                            # "Total redup_reddown",
                            # "Local redup_reddown",
                        ],
                        [
                            self.auction_data['expenses']["local"].loc[market_operator],
                            self.auction_data["expenses"][
                                "liftup_liftdown"
                            ].loc[market_operator],
                            # self.auction_data["expenses"][
                            #     "liftup_reddown"
                            # ].loc[market_operator],
                            self.auction_data["expenses"][
                                "redup_liftdown"
                            ].loc[market_operator],
                            # self.auction_data["expenses"][
                            #     "redup_reddown"
                            # ].loc[market_operator],
                        ],
                        [
                            self.auction_data['expenses']["local"].loc['total_expenses'],
                            self.auction_data["expenses"][
                                "liftup_liftdown"
                            ].loc["total_expenses"],
                            # self.auction_data["expenses"][
                            #     # "liftup_reddown"
                            # ].loc["total_expenses"],
                            self.auction_data["expenses"][
                                "redup_liftdown"
                            ].loc["total_expenses"],
                            # self.auction_data["expenses"][
                            #     "redup_reddown"
                            # ].loc["total_expenses"],
                        ],
                    ],
                    font=dict(size=20),
                    align="left",
                ),
            ),
            row=row,
            col=2,
        )
        return fig

    def generate_merit_order_of(self, market_operator: str, period: int):
        df_new_index, uniform_price, tender_amount = self._get_data_from_file(
            market_operator=market_operator, period=period
        )
        graph = self._make_merit_order(
            df_new_index=df_new_index,
            uniform_price=uniform_price,
            tender_amount=tender_amount,
        )
        return graph

    def _get_data_from_file(self, market_operator, period):
        """
        This script is used to visualize data from the database. To do so it first uses the methods of the module db_extract.py.
        The extracted data is saved in a dataframe and visualized using plotly graph objects.
        """
        # vis_logger = setup_logger('mas.visualisation', 'logs/visualisation.log')
        try:
            db = DataExtraction(
                self.filepath,
                market_operator=market_operator,
                period=period,
                write_all_data_to_excel=self.write_all_data_to_excel,
                write_mo_data_to_excel=self.write_mo_data_to_excel,
            )
            (
                auction_id,
                agents,
                bid_amount,
                bid_price,
                bid_ids,
                awarded_share,
                supply_start,
                tender_amount,
            ) = db.extract()
        except TypeError as e:
            vis_logger.debug(
                f" db_extract returned None values, there might not have been any bids in this period. "
                f"\nError message: {e}"
            )

        # save the data extracted from the database in a dataframe
        df = pd.DataFrame(
            {
                "Agents": agents,
                "Amount": bid_amount,
                "Price": bid_price,
                "Bid_Id": bid_ids,
                "Awarded_share": awarded_share,
            }
        )
        # print("df: ", df)

        # sort the dataframe by price
        df_sorted = df.sort_values(by=["Price"])
        # print("df sorted: ", df_sorted)
        # reset the index of the dataframe
        # this is necessary because we are later using the index to find out
        # the uniform_price
        df_new_index = df_sorted.reset_index(drop=True)
        # add column "Cumulative_capacity_MW" to the dataframe
        df_new_index["Cumulative_capacity_MW"] = df["Amount"].cumsum()

        # find out uniform price
        # returns all indices where cumulative capacity >= tender_amount
        indices = (
            df_new_index["Cumulative_capacity_MW"]
            .loc[lambda cumulative_amount: cumulative_amount >= tender_amount]
            .index
        )
        # take the price of the row where the tender_amount was satisfied first

        print("df new index: ", df_new_index)
        # old version was following line of code,
        # the indexing doesn't seem to work after integrating
        # SmartNordData and DER in db
        # uniform_price = df_new_index["Price"][indices[0]]
        uniform_price = df_new_index["Price"][0]

        return df_new_index, uniform_price, tender_amount

    def posneg_merit_order_list(self, periods: list):
        """Helper method to calculate merit orders of multiple time steps."""
        figs = []
        for period in periods:
            [pos_auction_data, neg_auction_data] = \
                self.extract_and_preprocess_multiple_auctions(period)
            self.auction_data = neg_auction_data
            fig = self._make_multiple_merit_order()
            figs.append(fig)
        return figs

    def posneg_merit_order_slider(self, periods: list):
        for period in periods:
            # self.auctionS hier zusammensetzen
            [pos_auction_data, neg_auction_data] = \
                self.extract_and_preprocess_multiple_auctions(period)
            self.pos_auctions_data[period] = pos_auction_data
            self.neg_auctions_data[period] = neg_auction_data
        # print(self.pos_auctions_data)
        # print(self.neg_auctions_data)
        pos = self.slider_make_multiple_merit_order(
                periods=periods,
                auctions_data=self.pos_auctions_data,
                title="Positive Regelreserve"
            )
        neg = self.slider_make_multiple_merit_order(
                periods=periods,
                auctions_data=self.neg_auctions_data,
                title="Negative Regelreserve"
            )
        
        return [
            pos,
            neg
        ]

    def generate_merit_order_slider(self, periods: list):
        for period in periods:
            # self.auctionS hier zusammensetzen
            self.extract_and_preprocess_data(period)
            self.auctions_data[period] = self.auction_data
            
        return self.slider_make_multiple_merit_order(
            periods=periods,
            auctions_data=self.auctions_data,
            title="Positive Regelreserve"
        )

    def slider_make_multiple_merit_order(self, periods, auctions_data, title):
        pd.set_option('display.max_rows', None)
        pd.set_option('display.max_columns', None)
        vis_logger.debug(f"Auctions data: {auctions_data}")
        # create mask for figure content
        period = periods[0]
        num_market_operators = len(auctions_data[period]["market_operators"])
        df_new_index=auctions_data[period]["bids"].loc[
                auctions_data[period]["bids"]["MarketOperator"] == 
                "MarketOperator_0"
            ]
        mask_fig = make_subplots(
            rows=num_market_operators,
            cols=1,
            shared_xaxes=False,
            # horizontal_spacing=0.03,
            vertical_spacing=0.25,
            specs=[[{"type": "bar"}]] * num_market_operators,
        )
        # edit layout
        mask_fig.update_layout(
            title=title,
            font_size=20,
        )
        if df_new_index.shape[0] > 0:
            sum_of_received_bids = \
                df_new_index[
                    "Cumulative_capacity_MW_connected"
                ].iloc[df_new_index.shape[0]-1]
        else:
            sum_of_received_bids = 0
        mask_fig.layout.xaxis.range = [
            -0.1, #1.5
            max(0.5, 1.5)#sum_of_received_bids * 4 / 3)
        ]
        mask_fig.layout.yaxis.range = [0.0, 1.0] #mask_fig.layout.yaxis.range
        if num_market_operators > 1:
            mask_fig.layout.xaxis2.range = [-0.1, 1.5] #mask_fig.layout.xaxis.range
            mask_fig.layout.yaxis2.range = [0.0, 1.0] #mask_fig.layout.yaxis.range
        if num_market_operators > 2:
            mask_fig.layout.xaxis3.range = [-0.1, 1.5] #mask_fig.layout.xaxis.range
            mask_fig.layout.yaxis3.range = [0.0, 1.0] #mask_fig.layout.yaxis.range
        
        # add axis title
        mask_fig.layout.xaxis.title = "Amount in MW"
        mask_fig.layout.yaxis.title = "Bid price<br>in EUR/MW"
        if num_market_operators > 1:
            mask_fig.layout.xaxis2.title = "Amount in MW"
            mask_fig.layout.yaxis2.title = "Bid price<br>in EUR/MW"
        if num_market_operators > 2:
            mask_fig.layout.xaxis3.title = "Amount in MW"
            mask_fig.layout.yaxis3.title = "Bid price<br>in EUR/MW"

        mask_fig.layout.xaxis.showgrid = True
        mask_fig.layout.yaxis.showgrid = True
        if num_market_operators > 1:
            mask_fig.layout.xaxis2.showgrid = True
            mask_fig.layout.yaxis2.showgrid = True
        if num_market_operators > 2:
            mask_fig.layout.xaxis3.showgrid = True
            mask_fig.layout.yaxis3.showgrid = True

        # make figure from mask
        fig_dict = mask_fig.to_dict()
        if "data" not in fig_dict.keys():
            fig_dict["data"] = []
        if "layout" not in fig_dict.keys():
            fig_dict["layout"] = {}
        if "frames" not in fig_dict.keys():
            fig_dict["frames"] = []

        # fill in most of layout
        fig_dict["layout"]["hovermode"] = "closest"
        fig_dict["layout"]["updatemenus"] = [
            {
                "buttons": [
                    {
                        "args": [None, {"frame": {"duration": 500, "redraw": False},
                                        "fromcurrent": True, "transition": {"duration": 500,
                                                                            "easing": "quadratic-in-out"}}],
                        "label": "Play",
                        "method": "animate"
                    },
                    {
                        "args": [[None], {"frame": {"duration": 0, "redraw": False},
                                        "mode": "immediate",
                                        "transition": {"duration": 0}}],
                        "label": "Pause",
                        "method": "animate"
                    }
                ],
                "direction": "left",
                "pad": {"r": 10, "t": 87},
                "showactive": False,
                "type": "buttons",
                "x": 0.1,
                "xanchor": "right",
                "y": 0,
                "yanchor": "top"
            }
        ]

        sliders_dict = {
            "active": 0,
            "yanchor": "top",
            "xanchor": "left",
            "currentvalue": {
                "font": {"size": 20},
                "prefix": "Period: ",
                "visible": True,
                "xanchor": "right"
            },
            "transition": {"duration": 500, "easing": "cubic-in-out"},
            "pad": {"b": 10, "t": 50},
            "len": 0.9,
            "x": 0.1,
            "y": 0,
            "steps": []
        }

        # make data
        period = periods[0]
        axes = [('x', 'y')]
        if num_market_operators > 1:
            axes = [('x', 'y'), ('x2', 'y2')]
        if num_market_operators > 2:
            axes = [('x', 'y'), ('x2', 'y2'), ('x3', 'y3')]
        for mo in auctions_data[period]["market_operators"]:
            moidx = auctions_data[period]["market_operators"].index(mo)
            df_new_index=auctions_data[period]["bids"].loc[
                auctions_data[period]["bids"]["MarketOperator"] == mo
            ]
            # print(df_new_index)
            fig_dict = self.add_merit_order_traces(
                fig_dict, 
                df_new_index, 
                xaxis=axes[moidx][0], 
                yaxis=axes[moidx][1]
            )
        # work-around to have a sufficient number of traces
        traces_frame0 = auctions_data[period]["bids"].shape[0]
        max_number_of_traces = 0
        for period in periods:
            if max_number_of_traces < auctions_data[period]["bids"].shape[0]:
                max_number_of_traces = auctions_data[period]["bids"].shape[0]
        additional_traces = max_number_of_traces - traces_frame0
        fig_dict = self.add_dummy_traces(fig_dict, additional_traces)

        # make frames
        for period in periods:
            frame = {"data": [], "name": str(period)}
            for mo in auctions_data[period]["market_operators"]:
                moidx = auctions_data[period]["market_operators"].index(mo)
                df_new_index=auctions_data[period]["bids"].loc[
                        auctions_data[period]["bids"]["MarketOperator"] == 
                        mo
                    ]
                # print(df_new_index)
                frame = self.add_merit_order_traces(
                    frame, 
                    df_new_index, 
                    xaxis=axes[moidx][0], 
                    yaxis=axes[moidx][1]
                )
            fig_dict["frames"].append(frame)
            slider_step = {"args": [
                [period],
                {"frame": {"duration": 300, "redraw": False},
                "mode": "immediate",
                "transition": {"duration": 300}}
            ],
                "label": period,
                "method": "animate"
            }
            sliders_dict["steps"].append(slider_step)

        fig_dict["layout"]["sliders"] = [sliders_dict]

        fig = go.Figure(fig_dict)
        return fig

    def add_dummy_traces(self, data_dict, number_of_traces):
        if number_of_traces > 0:
            for i in range(number_of_traces):
                trace = go.Bar(
                    x=np.array(0),
                    y=np.array(0),
                    showlegend=False,
                    width=np.array(0.05),
                    hovertemplate="dummy"
                )
                data_dict["data"].append(trace)
        return data_dict

    def add_merit_order_traces(self, data_dict, df, xaxis=None, yaxis=None):
        for agent in range(0, len(df.index)):
            # add_trace needs single datapoints that's why we need to slice the dataframe one by one
            df_single_agent = df[agent : agent + 1]
            # print("this is the df_single_agent", df_single_agent)
            rgb_string = str(
                255
                * (
                    1
                    - df["Max_awarded_share"].iloc[agent] * 0.75
                    + 0.25
                )
            )
            color = self.rgb_to_rgba(f"rgb(70, {rgb_string}, 200)", 1.0)

            trace = go.Bar(
                x=df_single_agent["Cumulative_capacity_MW_connected"],
                y=df_single_agent["Price_EUR"],
                showlegend=False,
                # we need the offset to relocate the bars
                # it doesn't make much sense to offset them by the whole "Amount"-value,
                # but that's the way it works. Feel free to play around and see for yourself.
                offset=-df_single_agent["Amount_MW"],
                # sets the width of the bar so that it is correctly representing the bidded amount
                width=df_single_agent["Amount_MW"],
                # modify information showed on hovering bars. reads <html> tags. <br> = break line,
                # <extra></extra> at the end is used to deactivate the visualization of "trace_i" when hovering
                # which is just a bit more ugly
                hovertemplate="Bid id: {}<br>"
                "Amount (MW): {} <br>"
                "Price (€/MW): {} <br>"
                "Cumulative amount (MW): {} <br>"
                "Awarded share: {} <br>"
                "<extra></extra>"
                # values[0] because we refer to the first entry in the
                # column, otherwise we would have meta-information
                # included (e.g. "Name: Amount, dtype: int64"):
                .format(
                    df_single_agent["Bid_id"].values[0],
                    df_single_agent["Amount_MW"].values[0],
                    df_single_agent["Price_EUR"].values[0],
                    df_single_agent["Cumulative_capacity_MW_connected"].values[0],
                    round(df_single_agent["Awarded_share"].values[0], 3),
                ),
                marker_color=color,
            )
            
            if xaxis is not None:
                trace.xaxis = xaxis
            if yaxis is not None:
                trace.yaxis = yaxis
            data_dict["data"].append(trace)
        return data_dict
