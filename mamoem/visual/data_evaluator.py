import pandas as pd


class DataEvaluator:
    # ## Data manipulation/evaluation methods ## #

    def calculate_receiving_mo_number(auction_data):
        """Calculate for each bid, which MO receives it.
        """
        auction_data["bids"]["Receiving_mo_number"] = 0
        for bidx in range(auction_data["bids"].shape[0]):
            if auction_data["bids"].loc[bidx,"Receiving_mo_number"] == 0:
                relevant_rows = auction_data["bids"]["Bid_id"] == \
                    auction_data["bids"].loc[bidx, "Bid_id"]
                auction_data["bids"].loc[bidx, "Receiving_mo_number"] = \
                    len(auction_data["bids"].loc[relevant_rows, "MarketOperator"].unique())
        return auction_data
        
    def calculate_marginal_price(auction_data):
        # mo_marginal_prices = {}
        auction_data["marginal_price"] = pd.DataFrame(columns=["local", "connected"])

        auction_data["bids"]["Cumulative_capacity_MW_local"] = 0
        auction_data["bids"]["Cumulative_capacity_MW_connected"] = 0
        for mo in auction_data["market_operators"]:
            relevant_rows_connected = auction_data["bids"]["MarketOperator"] == mo
            if mo == "MarketOperator_0":
                relevant_rows_local = relevant_rows_connected & \
                    auction_data["bids"]["Receiving_mo_number"] == 1
            else:
                relevant_rows_local = relevant_rows_connected

            # print(auction_data)
            # # local
            mo_bids = auction_data["bids"].loc[relevant_rows_local]#.reset_index(drop=True)
            auction_data = DataEvaluator.get_marginal_price(
                auction_data, mo, mo_bids, column="local")

            # # connected
            mo_bids = auction_data["bids"].loc[relevant_rows_connected]#.reset_index(drop=True)
            auction_data = DataEvaluator.get_marginal_price(
                auction_data, mo, mo_bids, column="connected")

        return auction_data

    def get_marginal_price(auction_data, mo, mo_bids, column):
        tender_amount_reached = False
        marginal_price = 0
        if mo_bids.shape[0] > 0:
            mo_bids.loc[:, "Cumulative_amount_MW"] = mo_bids[
                "Amount_MW"
            ].cumsum()
            mo_bids.loc[:, "Awarded_share_"+column] = 0

            # loop over mo_bids until tender_amount is reached
            for bidx in range(mo_bids.shape[0]):
                if not tender_amount_reached:
                    if bidx == 0:
                        free_amount = auction_data['tender_amount'][mo]
                    else:
                        free_amount = auction_data['tender_amount'][mo] - \
                            mo_bids["Cumulative_amount_MW"].iloc[bidx-1]
                    mo_bids.loc[mo_bids.index[bidx], "Awarded_share_"+column] = \
                        min(1, free_amount/mo_bids["Amount_MW"].iloc[bidx])
                    marginal_price = mo_bids["Price_EUR"].iloc[bidx]
                    # stop calculation of awarded amount if tender_amount reached
                    if (
                        mo_bids["Cumulative_amount_MW"].iloc[bidx]
                        >= auction_data["tender_amount"][mo]
                    ):
                        tender_amount_reached = True
                else:
                    mo_bids.loc[mo_bids.index[bidx], "Awarded_share_"+column] = 0
            auction_data["bids"].loc[
                mo_bids.index, "Cumulative_capacity_MW_"+column
            ] = mo_bids["Cumulative_amount_MW"]
            auction_data["bids"].loc[
                mo_bids.index, "Awarded_share_"+column
            ] = mo_bids["Awarded_share_"+column]
                    
        auction_data["marginal_price"].loc[mo, column] = marginal_price
        return auction_data

    def calculate_max_awarded_amount(auction_data):
        auction_data["bids"]["Max_awarded_share"] = 0
        # loop over bids to find maximal awarded amount
        for bidx in range(auction_data["bids"].shape[0]):
            relevant_rows = (
                auction_data["bids"]["Bid_id"]
                == auction_data["bids"]["Bid_id"].iloc[bidx]
            )
            awarded_amount = auction_data["bids"].loc[
                relevant_rows, "Awarded_share"
            ]
            auction_data["bids"].loc[
                relevant_rows, "Max_awarded_share"
            ] = awarded_amount.max()

        return auction_data

    def calculate_mo_expenses(auction_data, calculate_scenarios=False):
        """Calulate expenses for uniform pricing auction by multiplying
        tender amount and marginal price for each market operator.
        """
        expenses = pd.DataFrame(columns=["local"])
        expenses.loc["total_expenses"] = 0
        # sum up tender amount times marginal price
        for mo in auction_data["market_operators"]:
            expenses.loc[mo] = 0
            relevant_rows = auction_data["bids"]["MarketOperator"] == mo
            for bid in auction_data["bids"].loc[relevant_rows].iterrows():
                if not pd.isna(bid[1]["Awarded_share_local"]):
                    expenses.loc[mo] += auction_data["marginal_price"].loc[mo, "local"] \
                        * bid[1]["Awarded_share_local"] \
                        * bid[1]["Amount_MW"]

            expenses["local"].loc[mo] = round(expenses["local"].loc[mo], 3)
            expenses["local"].loc["total_expenses"] += expenses["local"][mo]
        
        if calculate_scenarios:
            expenses = DataEvaluator._calculate_scenario_expenses(
                auction_data, expenses
            )
        expenses = round(expenses, 3)  # round to three digits

        auction_data["expenses"] = expenses
        return auction_data

    def _calculate_scenario_expenses(auction_data, expenses):
        expenses["liftup_liftdown"] = 0
        expenses["liftup_reddown"] = 0
        expenses["redup_liftdown"] = 0
        expenses["redup_reddown"] = 0

        # liftup_liftdown
        mprice = {}
        for mo in auction_data["market_operators"]:
            if mo == "MarketOperator_0":  # find max marginal price globally
                mprice[mo] = max(auction_data["marginal_price"]["connected"])
            else:  # find max marginal price locally or at upstream mo_0
                mprice[mo]= max(
                    auction_data["marginal_price"].loc["MarketOperator_0", "connected"],
                    auction_data["marginal_price"].loc[mo, "connected"],
                )
        expenses = DataEvaluator.calculate_cumulative_bid_expenses(
            auction_data, 
            expenses, 
            column="liftup_liftdown", 
            mo_marginal_prices=mprice
        )

        # liftup_reddown
        mprice = {}
        for mo in auction_data["market_operators"]:
            if mo == "MarketOperator_0":  # find max marginal price globally
                mprice[mo] = max(auction_data["marginal_price"]["connected"])
            else:  # keep marginal price locally
                mprice[mo] = auction_data["marginal_price"].loc[mo, "connected"]
        expenses = DataEvaluator.calculate_cumulative_bid_expenses(
            auction_data, 
            expenses, 
            column="liftup_reddown", 
            mo_marginal_prices=mprice
        )            

        # redup_liftdown
        mprice = {}
        for mo in auction_data["market_operators"]:
            if mo == "MarketOperator_0":  # keep marginal price
                mprice[mo] = auction_data["marginal_price"].loc[mo,"connected"]
            else:  # find max marginal price locally or at upstream mo_0
                mprice[mo]= max(
                    auction_data["marginal_price"].loc["MarketOperator_0", "connected"],
                    auction_data["marginal_price"].loc[mo, "connected"],
                )
        expenses = DataEvaluator.calculate_cumulative_bid_expenses(
            auction_data, 
            expenses, 
            column="redup_liftdown", 
            mo_marginal_prices=mprice
        )

        # redup_reddown
        mprice = {}
        for mo in auction_data["market_operators"]:
            if mo == "MarketOperator_0":  # keep marginal price
                mprice[mo] = auction_data["marginal_price"].loc[mo,"connected"]
            else:  # keep marginal price locally
                mprice[mo] = auction_data["marginal_price"].loc[mo, "connected"]
        expenses = DataEvaluator.calculate_cumulative_bid_expenses(
            auction_data, 
            expenses, 
            column="redup_reddown", 
            mo_marginal_prices=mprice
        )

        # total expenses
        expenses.loc["total_expenses"] = expenses.sum(axis=0)

        return expenses

    def calculate_cumulative_bid_expenses(auction_data, expenses, column, mo_marginal_prices):
        exp_per_bid = pd.DataFrame(columns=["expenses"])
        for bid in auction_data["bids"].iterrows():
            mo = bid[1]["MarketOperator"]
            exp_per_bid.loc[bid[0]] = mo_marginal_prices[mo] \
                    * bid[1]["Awarded_share_connected"] \
                    * bid[1]["Amount_MW"]
            
            if mo == "MarketOperator_0":
                expenses.loc[mo, column] += \
                    exp_per_bid.loc[bid[0], "expenses"]
            else:
                other_mo_rows = auction_data["bids"]["MarketOperator"] != mo
                same_bid_rows = \
                    auction_data["bids"]["Bid_id"] == bid[1]["Bid_id"]
                already_covered_costs = 0
                try:
                    already_covered_costs = max(exp_per_bid.loc[same_bid_rows & other_mo_rows, "expenses"])
                except:
                    continue
                
                expenses.loc[mo, column] += \
                    max(0, 
                        exp_per_bid.loc[bid[0], "expenses"]
                        - already_covered_costs
                    )
        return expenses