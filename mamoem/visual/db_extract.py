import os
import json
import pandas as pd
import openpyxl
from pathlib import Path
from mamoem.util.logger import setup_logger

vis_logger = setup_logger('mas.visualisation', 'logs/visualisation.log')


def selected_mo_db_to_excel(mo_ordered):
    """
        This method is used to write the data that we are visualizing 
        into a separate excel
     """
    # following code can be used to verify/lookup the data saved in the dataframe in excel
    filepath_xlsx = Path('data/db_entries_selected_mo.xlsx')
    if os.path.exists(filepath_xlsx):
        os.remove(filepath_xlsx)
    df_mo_ordered = pd.DataFrame(mo_ordered)
    df_mo_ordered.to_excel(filepath_xlsx)


def get_data_for_key(db_key, table):
    """
    Gets the data for requested db_key from HDF-database
    :param db_key: str: are we going for e.g. DER data, MAS data or SmartNordData?
    :param table: pandas.io.pytables.HDFStore
    """
    data_keys = table.keys()
    try:
        for key in data_keys:
            if key == db_key:
                data = table[key]
                return data
    except Exception as e:
        vis_logger.debug(f"There don't seem to be any bids in this period. Error message: {e}")


def print_column_data(column_specifier, dataframe):
    """
    Prints out the data in (a) certain column(s) of a dataframe
    :param column_specifier: str: which column(s) do we want to get data from
    :param dataframe: DataFrame: which dataframe are we scanning for data
    """
    for column in dataframe:
        if column_specifier in column:
            print("Data in ", dataframe.name, "dataframe for ", column_specifier, "column(s): ")
            for row in dataframe[column]:
                print(row)


class DataExtraction:
    """
    This class is used to extract data from the database.
    :param filepath: str: indicates the path where the database is saved
    :param market_operator: str: tells us which market_operators data we want to extract
    :param period: int: tells us in which period in the database we have to look
    :param write_all_data_to_excel: writes all the data from the database into an excel-file
    :param write_mo_data_to_excel: writes the data from the selected market_operator into an excel-file
    """
    def __init__(self, filepath: str,
                 period: int = None,
                 market_operator: str = None,
                 write_all_data_to_excel=True,
                 write_mo_data_to_excel=True):
        self.filepath = filepath
        self.filepath_xlsx = Path('data/db_all_entries.xlsx')
        if os.path.exists(self.filepath_xlsx):
            os.remove(self.filepath_xlsx)
        self.xlsx_writer = pd.ExcelWriter(self.filepath_xlsx)
        self.market_operator = market_operator
        # self.period = period
        self.write_all_data_to_excel = write_all_data_to_excel
        self.write_mo_data_to_excel = write_mo_data_to_excel

        # table is of type <class 'pandas.io.pytables.HDFStore'>
        # until "data=" is just to get the data from the database, not much to quirk around with here
        self.table = pd.HDFStore(self.filepath)
        # careful: table.keys() returns a list

    def extract_socs(self, periods: list):
        db_key = '/DER__0'
        
        bat_df = get_data_for_key(db_key=db_key, table=self.table)
        bat_df.name = db_key

        entities = self.get_entities(bat_df)
        # read keys of batteries
        batteries = self.filter_entities(entities, 'Battery')
        # print(bat_df)
        socs = {}
        p_kw = {}

        for battery in batteries:
            socs[battery] = list(bat_df[battery+'___soc_percent'][periods])
            p_kw[battery] = list(bat_df[battery+'___p_kw'][periods])

        battery_data = {
            "batteries": batteries,
            "socs": socs,
            "p_kw": p_kw
        }
        return battery_data

    def extract_p_set_kw(self, periods: list):
        db_key = '/MAS__0'
        
        mas_df = get_data_for_key(db_key=db_key, table=self.table)
        mas_df.name = db_key

        entities = self.get_entities(mas_df)
        # read keys of market operators
        market_participants = self.filter_entities(entities, 'MarketParticipant')
        # print(mas_df)
        p_set_kw = {}

        for mp in market_participants:
            p_set_kw[mp] = list(mas_df[mp+'___p_set_kw'][periods])
        num_market_operators = len(self.filter_entities(entities, 'MarketOperator'))

        market_participants_data = {
            "p_set_kw": p_set_kw,
            "enum_offset": num_market_operators
        }
        return market_participants_data

    def extract_auction(self, period: int):
        db_key = '/MAS__0'
        
        mas_df = get_data_for_key(db_key=db_key, table=self.table)
        mas_df.name = db_key

        entities = self.get_entities(mas_df)
        # read keys of market operators
        market_operators = self.get_market_operators(entities)

        # auction_data has the following structure:
        # auction_data = {'auction_id': str, 
        #     'market_operators': [mo1, mo2, ...], 
        #     'tender_amount': {mo1: ta1, mo2: ta2, ...},
        #     'bids': DataFrame}
        auction_data = self.get_auction_data(mas_df=mas_df, period=period, market_operators=market_operators)
        return auction_data

    def get_auction_data(self, mas_df, period, market_operators):
        auction_id = None
        tender_amount = {}
        bids = pd.DataFrame(columns=["MarketOperator", "Bid_id", "Amount_MW", "Price_EUR", "Awarded_share"])
        # print(mas_df)

        for market_operator in market_operators:

            auction_params_json = json.loads(mas_df[market_operator+'___auction_id_tender_amount_supply_start'].loc[period])
            auction_bids_json = json.loads(mas_df[market_operator+'___auction_id_bids'].loc[period])
            # print(auction_params_json)
            # print(auction_bids_json)
            if auction_id is None:
                auction_id = auction_params_json[0]
            else:
                assert auction_id == auction_params_json[0], \
                    f"TODO: Integrate multiple auctions " \
                    f"at the same period of time."
            tender_amount[market_operator] = auction_params_json[1]
            for bid_json in auction_bids_json[1]:
                bid_list = [
                    market_operator, 
                    bid_json[3], 
                    bid_json[1], 
                    bid_json[2], 
                    bid_json[4]]
                bids.loc[len(bids)] = bid_list
        auction_data = {
            "auction_id": auction_id,
            "market_operators": market_operators,
            "tender_amount": tender_amount,
            "bids": bids
        }
        # print(auction_data)
        return auction_data

    def extract_multiple_auctions(self, period: int):
        db_key = '/MAS__0'
        
        mas_df = get_data_for_key(db_key=db_key, table=self.table)
        mas_df.name = db_key

        entities = self.get_entities(mas_df)
        # read keys of market operators
        market_operators = self.get_market_operators(entities)
        auctions = self.get_auctions(
            df=mas_df, period=period, market_operators=market_operators)

        # auction_data has the following structure:
        # auction_data = { 
        #     'auction_type': {
        #         'auction_id': str, 
        #         'market_operators': [mo1, mo2, ...], 
        #         'tender_amount': {mo1: ta1, mo2: ta2, ...},
        #         'bids': DataFrame
        #     }
        # }
        auction_data = self.get_multiple_auctions_data(
            mas_df=mas_df, 
            period=period, 
            market_operators=market_operators,
            auctions=auctions
        )
        return auction_data

    def get_multiple_auctions_data(self, mas_df, period, market_operators, auctions):
        auction_data = {}
        for (auction_id, auction_type) in auctions:
            tender_amount = {}
            bids = pd.DataFrame(columns=["MarketOperator", "Bid_id", "Amount_MW", "Price_EUR", "Awarded_share"])
            # print(mas_df)

            for market_operator in market_operators:

                auction_params_json = json.loads(mas_df[market_operator+'___auction_id_tender_amount_supply_start'].loc[period])
                auction_bids_json = json.loads(mas_df[market_operator+'___auction_id_bids'].loc[period])
                # print(auction_params_json)
                # print(auction_bids_json)
                tender_amount[market_operator] = auction_params_json[
                    auction_params_json.index(auction_id) + 1
                ]
                idx_bids_start = auction_bids_json.index(auction_id)
                for bid_json in auction_bids_json[idx_bids_start+1]:
                    bid_list = [
                        market_operator, 
                        bid_json[3], 
                        bid_json[1], 
                        bid_json[2], 
                        bid_json[4]]
                    bids.loc[len(bids)] = bid_list
            auction_data[auction_type] = {
                "auction_id": auction_id,
                "market_operators": market_operators,
                "tender_amount": tender_amount,
                "bids": bids
            }
            # print(auction_data[auction_type])
        return auction_data
        

    # def extract(self):

    #     db_key = '/MAS__0'
    #     # table is of type <class 'pandas.io.pytables.HDFStore'>
    #     # until "data=" is just to get the data from the database, not much to quirk around with here
    #     table = pd.HDFStore(self.filepath)
    #     # careful: table.keys() returns a list
    #     db_df = get_data_for_key(db_key=db_key, table=table)
    #     db_df.name = db_key

    #     print_column_data(self.market_operator, db_df)
    #     if self.write_all_data_to_excel:
    #         self.db_to_excel(key=db_key, data=db_df)
    #     self.xlsx_writer.save()

    #     # ## Procedure ## #

    #     mo_ordered = []
    #     for market_operator in db_df.columns:
    #         # iterate over all columns and take the data for the respective market_operator
    #         if self.market_operator in market_operator:
    #             # is saved without the column-name in the list
    #             mo_ordered.append(db_df[market_operator])
    #     print("mo ordered: ", mo_ordered)
    #     # ! WRITING TO EXCEL IN ALL CASES SUDDENLY PRODUCES AN EXCEL-ERROR - CORRUPTED FILE/FORMAT
    #     # ! THis was a new attempt at looking at the data
    #     self.filepath_xlsx = Path('data/mo ordered.xlsx')
    #     # os.remove(self.filepath_xlsx)
    #     self.xlsx_writer = pd.ExcelWriter(self.filepath_xlsx)

    #     if self.write_mo_data_to_excel:
    #         selected_mo_db_to_excel(mo_ordered)

    #     # * [limitation] taking the respective mo-entry, this is currently hard-coded and works because in the current
    #     # design we are only ever searching/visualizing one market operator's auction and thus only
    #     # have two rows with one parameter each
    #     mo_auction_id_tender_amount_supply_start = mo_ordered[0]  # type = panda Series
    #     mo_auction_id_bids = mo_ordered[1]                        # type = panda Series

    #     # unloading the panda series information into lists
    #     json_mo_auction_id_tender_amount_supply_start = json.loads(mo_auction_id_tender_amount_supply_start[self.period])
    #     json_mo_auction_id_bids = json.loads(mo_auction_id_bids[self.period])  # type = list

    #     auction_id: str
    #     agent_ids = []
    #     bid_amounts = []
    #     bid_prices = []
    #     bid_ids = []
    #     awarded_share = []

    #     # "trying", because we don't know whether we actually have bid data in the respective period
    #     try:
    #         # constant variables in one auction (auction_id, supply_start, tender_amount)
    #         auction_id = json_mo_auction_id_bids[0]
    #         supply_start = json_mo_auction_id_tender_amount_supply_start[2]
    #         tender_amount = json_mo_auction_id_tender_amount_supply_start[1]
    #         # changing variables (bid information)
    #         for elem in json_mo_auction_id_bids[1]:
    #             agent_ids.append(elem[0][1])
    #             bid_amounts.append(elem[1])
    #             bid_prices.append(elem[2])
    #             bid_ids.append(elem[3])
    #             awarded_share.append(elem[4])
    #         return auction_id, agent_ids, bid_amounts, bid_prices, bid_ids, awarded_share, supply_start, tender_amount
    #     except UnboundLocalError as e:
    #         vis_logger.debug(f"Period without entries? Supply start and tender amount defined? Error message: {e}")
    #     except IndexError as e:
    #         vis_logger.debug(f"There don't seem to be any bids in this period. Error message: {e}")
    #     finally:
    #         table.close()

    def db_to_excel(self, key='unnamed_sheet', data=None):
        """
        This method is used to write the data from the hdf5 database/file to an excel in a specified path
        """
        print(key.strip('[]:*?/\\'))
        data.to_excel(self.xlsx_writer, sheet_name=key.strip('[]:*?/\\'))
        
    def get_entities(self, df):
        entities = []
        for column in df.columns:
            elements = column.split('___')
            # print(elements)
            entities.append(elements[0])
        # remove duplicate values
        ent_set = set(entities)
        
        return list(ent_set)
    
    def get_market_operators(self, entities):
        market_operators = []
        for ent in entities:
            if 'MarketOperator' in ent:
                market_operators.append(ent)
        return sorted(market_operators)

    def filter_entities(self, entities, filter_string):
        filtered_entities = []
        for ent in entities:
            if filter_string in ent:
                filtered_entities.append(ent)
        return sorted(filtered_entities)

    def get_auctions(self, df, period, market_operators):
        auctions = []
        sorted_mos = sorted(market_operators)
        auction_id_type = json.loads(
            df[sorted_mos[0]+'___auction_id_type'].loc[period]
        )
        assert len(auction_id_type) % 2 == 0
        for idx in range(int(len(auction_id_type)/2)):
            # auctions = [{
            # auction_id: auction_type
            # }]
            auctions.append((
                auction_id_type[2*idx], auction_id_type[2*idx+1]
            ))
        return auctions

# if __name__ == '__main__':
#     main()


