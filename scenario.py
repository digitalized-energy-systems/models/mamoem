"""
This file contains the mosaik scenario.  To start the simulation, just run this
script from the command line::

    $ python scenario.py

Since neither the simulator in ``src/wecssim`` nor the MAS in ``src/mas`` are
installed correctly, we add the ``src/`` directory to the PYTHONPATH so that
Python will find these modules.


"""
import sys
import mosaik
import logging

from scenario.generate_config import ConfigMaker
from midas.util.runtime_config import RuntimeConfig

logging.basicConfig(filename="logs/log.log", level=logging.DEBUG)

# logging.getLogger("pysimmods").setLevel(logging.DEBUG)
# logging.getLogger("pysimmods").addHandler(
#     logging.FileHandler("logs/pysimmods.log")
# )  # remove this lines to see pysimmods output in console

# We have the WECS simulator, a multi-agent system, and a database:
SIM_CONFIG = {
    "MAS": {
        # Again, we need to extend the PYTHONPATH so that Python can find our
        # packages:
        "cmd": "%(python)s -m mas.mosaik  %(addr)s",
        "env": {
            "PYTHONPATH": "mamoem/",
        },
        # 'python': 'mas.mosaik:MosaikAPI'
        # Newer versions of mosaik might favor this new-style formatting:
        # 'cmd': '{python} -m mas.mosaik -l debug {addr}',
        # 'env': {
        #     'PYTHONPATH': 'src/',
        # },
    },
    "DER": {"python": "pysimmods.mosaik.flex_mosaik:FlexibilitySimulator"},
    "DB": {
        # The HDF5 db should also be run as a separate process:
        # 'cmd': 'mosaik-hdf5 %(addr)s',
        "python": "midas.modules.store.simulator:MidasHdf5"
    },
    "SmartNordData": {
        "python": "midas.modules.sndata.simulator:SmartNordDataSimulator"
    },
    "VEAData": {
        "python": "veadata.simulator:VEADataSimulator"
    },
}

# We now define some constants with configuration for the simulator and the
# MAS.  This way, we can easily see and edit the config values without touching
# the actual scenario:

# Simulation duration:
DURATION = 3600 * 2 * 1  # 1 hour


MAS_CONFIG = ConfigMaker.make_mas_config()

assert (MAS_CONFIG["host_mosaik"], MAS_CONFIG["port_mosaik"]) != (
    MAS_CONFIG["host_mango"],
    MAS_CONFIG["port_mango"],
)
# decide whether to use smartnord data or vea data
use_smart_nord_data = False # True #

# Market Operator:
MO_CONFIG = ConfigMaker.make_mo_config()

# Market participants and devices 
num_mps = 9
if use_smart_nord_data:
    sgl_list = ConfigMaker.return_tmp_indice(num_mps)

else:
    # sgl_list contains one element for each market participant
    sgl_list = ConfigMaker.load_site_grid_level_list(
        './scenario/indexes_capacities_load_profiles.csv', num_mps)


DER_CONFIG = ConfigMaker.make_der_config()
MP_CONFIG, BAT_CONFIG = ConfigMaker.make_mp_bat_config(sgl_list, use_smart_nord_data)

sum_mp = 0
for num_mp, _ in MP_CONFIG:
    sum_mp = sum_mp + num_mp
assert sum_mp <= num_mps, f'sum_mp: {sum_mp}, num_mp: {num_mps}'

# Database config
DB_PATH = "data/mosaik_results.hdf5"


def main():
    """Compose the mosaik scenario and run the simulation."""
    # We first create a World instance with our SIM_CONFIG:
    world = mosaik.World(SIM_CONFIG)

    # We then start the MAS simulator and pass the "init()" params
    # to them:
    mas = world.start("MAS", mas_config=MAS_CONFIG)
    print(type(mas))

    market_operators = []
    for n_mo, params in MO_CONFIG:  # Iterate over the config sets
        for _ in range(n_mo):
            mo = mas.MoAgent(**params)
            # Remember the mo entity for connecting it to the DB later:
            market_operators.append(mo)

    market_participants = []
    for n_mp, params in MP_CONFIG:  # Iterate over the config sets
        for _ in range(n_mp):
            mp = mas.MpAgent(**params)
            market_participants.append(mp)

    # start batteries
    bats = world.start("DER", **DER_CONFIG)
    batteries = []
    for n_bat, params in BAT_CONFIG:  # Iterate over the config sets
        for _ in range(n_bat):
            bat = bats.Battery(**params)
            batteries.append(bat)

    loads = []
    if not use_smart_nord_data:
        # start simulator for VEA load data
        VEA_CONFIG = ConfigMaker.make_vea_config()
        veadata = world.start("VEAData",**VEA_CONFIG)
        # create load simulator for every mp
        loads = []
        for agent in range(len(market_participants)):
            load = veadata.IndustrialForecast(
                sgl=sgl_list[agent], scaling=1.0, forecast_horizon_hours=2
            )
            loads.append(load)
    else:
        # start simulator for Smart Nord load data
        # MAS config
        START_DATE = ConfigMaker.return_start_date()
        sndata = world.start(
            "SmartNordData",
            step_size=900,
            start_date=START_DATE,
            data_path=RuntimeConfig().paths["data_path"],
            filename="SmartNordProfiles.hdf5",
        )
        # create load simulator for every mp
        loads = []
        for agent in range(len(market_participants)):
            load = sndata.HouseholdForecast(
                eidx=agent, scaling=1.1, forecast_horizon_hours=2
            )
            loads.append(load)

    # Start the database process
    db = world.start("DB", step_size=60 * 15, duration=DURATION)
    hdf5 = db.Database(filename=DB_PATH)

    # Connect market participants and batteries
    for agent in range(len(market_participants)):
        world.connect(
            batteries[agent],
            market_participants[agent],
            "soc_percent",
            time_shifted=True,
            initial_data={
                "soc_percent": BAT_CONFIG[agent][1]['inits']['soc_percent']
            },
        )
        world.connect(market_participants[agent], batteries[agent], "p_set_kw")
        world.connect(
            loads[agent], market_participants[agent], "p_mw_forecast"
        )

    # Connect entities to database:
    mosaik.util.connect_many_to_one(
        world,
        market_operators,
        hdf5,
        "auction_id_type",
        "auction_id_tender_amount_supply_start",
        "auction_id_bids",
    )
    mosaik.util.connect_many_to_one(
        world, market_participants, hdf5, "p_set_kw"
    )
    mosaik.util.connect_many_to_one(
        world, batteries, hdf5, "soc_percent", "p_kw"
    )

    mosaik.util.connect_many_to_one(world, loads, hdf5, "p_mw_forecast")

    # Run the simulation
    world.run(DURATION)


if __name__ == "__main__":
    main()
