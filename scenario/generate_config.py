import csv
import pandas as pd
import random

# MAS config
START_DATE = "2016-01-01 00:00:00+00:00"  # CET
DATE_FORMAT = "%Y-%m-%d %H:%M:%S%z"

# Multi Agent System
HOST_MANGO = "localhost"
PORT_MANGO = 5558

# Market Operators
TSMO_EID = "MarketOperator_0"
DSMO_EID1 = "MarketOperator_1"
DSMO_EID2 = "MarketOperator_2"

BATTERY_DATA = pd.HDFStore('./data/industrial_consumers_bess.hdf5')

SEED = 112


class ConfigMaker:
    def make_mas_config():
        mas_config = {
            "host_mosaik": "localhost",
            "port_mosaik": 5557,
            "host_mango": HOST_MANGO,
            "port_mango": PORT_MANGO,
            "start_date": START_DATE,
            "date_format": DATE_FORMAT,
            "seed": SEED,
        }
        return mas_config

    def load_site_grid_level_list(file_path, num_profiles):
        csvfile = open(file_path, 'r', newline='')
        reader = csv.reader(csvfile, delimiter=',', )

        sgl_list = []
        profile_ctr = 0
        for row in reader:
            profile_ctr = profile_ctr + 1
            if profile_ctr > num_profiles:
                break
            sgl_list.append(row[0])
        return sgl_list

    def make_mo_config():

        mo_config = [
            (1, {"mo_eid": TSMO_EID}),
            (1, {'mo_eid': DSMO_EID1, 'upstream_mos': {TSMO_EID: 0.5}}),
            (1, {"mo_eid": DSMO_EID2, "upstream_mos": {TSMO_EID: 0.5}}),
        ]
        return mo_config

    def make_mp_bat_config(sgl_list, use_smart_nord_data=True):
        """
        Returns market participant and battery configuration. Either for smart nord data or for vea data.
        :param sgl_list: ConfigMaker: defines how many profiles are loaded
        :param use_smart_nord_data: Boolean: to determine whether smart nord data or vea data is used
        """
        mp_config = []
        bat_config = []
        site_ctr = 0
        rand_num_generator = random.Random(SEED+1)

        if use_smart_nord_data:
            for site in sgl_list:
                # read params
                # assemble MP
                divider = 3
                if site_ctr % divider == 0:
                    mo_eid = DSMO_EID1
                elif site_ctr % divider == 1:
                    mo_eid = DSMO_EID2
                else:
                    mo_eid = TSMO_EID
                site_ctr +=1

                # value of this dictionary are used for mp and battery configuration
                mp_bat_data = {
                    "p_max_grid_set_kw": 10,
                    "cap_kwh": 20,
                    "p_charge_max_kw": 20,
                    "p_discharge_max_kw": 20,
                    "eta_ch": 0.94,
                    "eta_dis": 0.94,
                    "soc_min_percent": 0,
                    "eta_pc": [-2.109566, 0.403556, 97.110770],
                    "soc_percent": 50,
                }

                mp_c = (1,
                        {
                            "mo_addr": (HOST_MANGO, PORT_MANGO),
                            "mo_eid": mo_eid,
                            "p_max_grid_set_kw": mp_bat_data['p_max_grid_set_kw'],
                            "cap_kwh": mp_bat_data['cap_kwh'],
                            "p_charge_max_kw": mp_bat_data['p_charge_max_kw'],
                            "p_discharge_max_kw": mp_bat_data['p_discharge_max_kw'],
                            "eta_ch": mp_bat_data['eta_ch'],
                            "eta_dis": mp_bat_data['eta_dis'],
                        })

                mp_config.append(mp_c)
                # assemble BAT
                bat_c = (1, {
                    "params": {
                        "cap_kwh": mp_bat_data['cap_kwh'],
                        "p_charge_max_kw": mp_bat_data['p_charge_max_kw'],
                        "p_discharge_max_kw": mp_bat_data['p_discharge_max_kw'],
                        "soc_min_percent": mp_bat_data['soc_min_percent'],
                        "eta_pc": mp_bat_data['eta_pc'],
                    },
                    "inits": {"soc_percent": 50},
                })
                bat_config.append(bat_c)
        else:
            for site in sgl_list:
                # read params
                bat_params = BATTERY_DATA["/bess_liion/peak_shaving"].loc[site]
                # assemble MP
                divider = 3
                if site_ctr % divider == 0:
                    mo_eid = DSMO_EID1
                elif site_ctr % divider == 1:
                    mo_eid = DSMO_EID2
                else:
                    mo_eid = TSMO_EID
                site_ctr +=1

                mp_c = (1, {
                    "mo_addr": (HOST_MANGO, PORT_MANGO),
                    "mo_eid": mo_eid,
                    "p_max_grid_set_kw": bat_params['Max_grid_power_MW']*1e3,
                    "cap_kwh": bat_params['Energy_capacity_MWh']*1e3,
                    "p_charge_max_kw": bat_params['Max_battery_power_MW']*1e3,
                    "p_discharge_max_kw": bat_params['Max_battery_power_MW']*1e3,
                    "eta_ch": bat_params['Discharge_efficiency'],
                    "eta_dis": bat_params['Discharge_efficiency'],
                })
                mp_config.append(mp_c)
                # assemble BAT
                soc_start = bat_params['Initial_state_of_energy']*100
                # for distribution
                soc_start = soc_start + (100-soc_start) * rand_num_generator.random()
                bat_c = (1, {
                    "params": {
                        "cap_kwh": bat_params['Energy_capacity_MWh']*1e3,
                        "p_charge_max_kw": bat_params['Max_battery_power_MW']*1e3,
                        "p_discharge_max_kw": bat_params['Max_battery_power_MW']*1e3,
                        "soc_min_percent": 0,
                        "eta_pc": [0, 0, bat_params['Discharge_efficiency']*100],
                    },
                    "inits": {
                        "soc_percent": soc_start
                    },
                })
                bat_config.append(bat_c)
        return mp_config, bat_config

    def make_der_config():
        return {"start_date": START_DATE}

    def make_vea_config():
        vea_config = {
            'step_size': 900,
            'start_date': START_DATE,
            'data_path': "./data/",
            'filename': "industrial_load.hdf5",
        }
        return vea_config

    def return_tmp_indice(num:int):
        """
        Sole purpose of this method is to suppress the type-warning "expected type ConfigMaker, got "list" instead",
        when calling "ConfigMaker.make_mp_bat_config(sgl_list, use_smart_nord_data)" in scenario.py
        Apparently works, because this gives a list back from the class ConfigMaker.
        """
        tmp_indice = list(range(num))
        return tmp_indice

    def return_start_date():
        """
        Returns the start date.
        """
        return START_DATE
