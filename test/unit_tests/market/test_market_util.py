import pytest
import sys
from mamoem.mas.market.market_util import *
import numpy as np

def test_bid_storage():

    bid_storage = MarketManager('agent_test', 10)
    bid_storage.update_bids(900)
    
    # without bid
    pos_mpo, neg_mpo = bid_storage.get_mpo_vectors(forecast_horizon_index=4)
    assert((pos_mpo == numpy.zeros(4)).all())
    assert((neg_mpo == numpy.zeros(4)).all())

    # single bid
    bid1 = Bid(
        auction_id='1',
        product_type='balancing_reserve',
        power_flow_consumption=True,
        supply_start=1800,
        supply_duration=1800,
        amount_mw=0.5,
        price=10
    )
    bid_storage.add_bid(bid1)
    pos_mpo, neg_mpo = bid_storage.get_mpo_vectors(forecast_horizon_index=4)

    assert(pos_mpo[1] == 0.5*1e6)
    assert(pos_mpo[2] == 0.5*1e6)
    assert((neg_mpo == numpy.zeros(4)).all())

    # two bids in different directions
    bid2 = Bid(
        auction_id='2',
        product_type='balancing_reserve',
        power_flow_consumption=False,
        supply_start=3600,
        supply_duration=1800,
        amount_mw=0.5,
        price=10
    )
    bid_storage.add_bid(bid2)
    pos_mpo, neg_mpo = bid_storage.get_mpo_vectors(forecast_horizon_index=8)

    # bid at end of forecast horizon
    pos = np.array([0, 0.5*1e6, 0.5*1e6, 0, 0, 0, 0, 0])
    neg = np.array([0, 0, 0, -0.5*1e6, -0.5*1e6, 0, 0, 0])
    # print(pos, pos_mpo)
    # print(neg, neg_mpo)
    assert((pos_mpo == pos).all())
    assert((neg_mpo == neg).all())

    bid3 = Bid(
        auction_id='3',
        product_type='balancing_reserve',
        power_flow_consumption=False,
        supply_start=7200,
        supply_duration=1800,
        amount_mw=0.5,
        price=10
    )
    bid_storage.add_bid(bid3)
    pos_mpo, neg_mpo = bid_storage.get_mpo_vectors(forecast_horizon_index=8)

    pos = np.array([0, 0.5*1e6, 0.5*1e6, 0, 0, 0, 0, 0])
    neg = np.array([0, 0, 0, -0.5*1e6, -0.5*1e6, 0, 0, -0.5*1e6])
    assert((pos_mpo == pos).all())
    assert((neg_mpo == neg).all())
