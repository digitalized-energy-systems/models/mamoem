"""This module contains the meta for the industrial load data simulator.

It is based on the VEA load data, which is stored partially in a data base.
Specific keys can be extracted in order to access distinct profiles.
"""

META = {
    "type": "time-based",
    "models": {
        "IndustrialForecast": {
            "public": True,
            "params": [
                "scaling",
                "sgl",  # site grid level
                "interpolate",
                "randomize_data",
                "forecast_horizon_hours",
            ],
            "attrs": ["cos_phi", "p_mw_forecast", "q_mvar_forecast"],
        },
    },
}
