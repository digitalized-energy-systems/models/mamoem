import os
import mosaik_api
import pandas as pd

from midas.util.base_data_model import DataModel
from midas.util.forecast_data_model import ForecastDataModel
from midas.util.base_data_simulator import BaseDataSimulator
from midas.util.print_format import mformat

from mamoem.util.logger import setup_logger

from .meta import META

veadata_logger = setup_logger('mosaik.veadata', 'logs/veadata.log', propagate=False)

class VEADataSimulator(BaseDataSimulator):
    """A simulator for the VEA load data."""

    def __init__(self):
        super().__init__(META)

        self.load_p = None
        self.load_q = None

        self.num_models = dict()
        self.industrial_ctr = 0
        self.num_industrials = 0

    def init(self, sid, **sim_params):
        """Called exactly once after the simulator has been started.

        :return: the meta dict (set by mosaik_api.Simulator)
        """
        super().init(sid, **sim_params)

        # Load the data
        data_path = sim_params.get(
            "data_path",
            os.path.abspath(
                os.path.join(__file__, ".", "data")
            ),
        )
        file_path = os.path.join(
            data_path,
            sim_params.get(
                "filename", "industrial_load.hdf5"
            ),
        )
        veadata_logger.debug("Using db file at %s.", file_path)

        self.load_p = pd.read_hdf(file_path, "load_pmw")
        try:
            self.load_q = pd.read_hdf(file_path, "load_qmvar")
        except Exception:
            veadata_logger.debug("No q values for loads available. Skipping.")

        self.num_industrials = len(self.load_p.columns)

        return self.meta


    def create(self, num, model, **model_params):
        """Initialize the simulation model instance (entity)

        :return: a list with information on the created entity
        """
        entities = list()
        self.num_models.setdefault(model, 0)
        for _ in range(num):
            eid = f"{model}-{self.num_models[model]}"

            if model == "IndustrialLoad":
                self.models[eid] = self._create_load(model_params)

            elif model == "IndustrialForecast":
                self.models[eid] = self._create_load_forecast(
                    model_params
                )

            else:
                raise AttributeError(f"Unknown model {model}.")

            self.num_models[model] += 1
            entities.append({"eid": eid, "type": model})

        return entities


    def step(self, time, inputs, max_advance=0):
        """Perform a simulation step."""
        if inputs:
            veadata_logger.debug("At step %d received inputs %s", time, mformat(inputs))

        return super().step(time, inputs, max_advance)

    def get_data(self, outputs):
        """Returns the requested outputs (if feasible)."""
        data = super().get_data(outputs)

        veadata_logger.debug(
            "At step %d gathered outputs %s", self._sim_time, mformat(data)
        )

        return data

    def _create_load(self, model_params):
        raise NotImplementedError()

    def _create_load_forecast(self, model_params):
        col = model_params.get("sgl", None)
        if col is None:
            idx = self.industrial_ctr
            self.industrial_ctr = (self.industrial_ctr + 1) % self.num_industrials
            col = self.load_p.columns[idx]
        # else:
            # idx = max(0, min(self.num_industrials, idx))
            # TODO: Check, whether col is in data base
        
        data_q = None
        if self.load_q is not None:
            data_q = self.load_q[col]

        model = ForecastDataModel(
            data_p=self.load_p[col],
            data_q=data_q,
            data_step_size=900,
            scaling=model_params.get("scaling", 1.0),
            seed=self.rng.randint(self.seed_max),
            # interpolate=model_params.get("interpolate", self.interpolate),
            interpolate=False,
            # randomize_data=model_params.get(
            #     "randomize_data", self.randomize_data
            # ),
            randomize_data=False,
            # randomize_cos_phi=model_params.get(
            #     "randomize_cos_phi", self.randomize_cos_phi
            # ),
            randomize_cos_phi=False,
            forecast_horizon_hours=model_params.get(
                "forecast_horizon_hours", 1.0
            ),
        )

        return model

if __name__ == "__main__":

    veadata_logger.info("Starting mosaik simulation...")
    mosaik_api.start_simulation(VEADataSimulator())