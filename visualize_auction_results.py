from mamoem.visual.auction_visualizer import *
import plotly
# import plotly.graph_objs as go
# plotly.offline.init_notebook_mode()

def main():
    va = AuctionVisualizer(
        # filepath="data/mosaik_results_168h_simplyfied_bidding.hdf5",
        # filepath="data/mosaik_results_simplbid_connected.hdf5",
        # filepath="data/mosaik_results_simplbid_local.hdf5",
        filepath="data/mosaik_results.hdf5",
        write_all_data_to_excel=True,
        write_mo_data_to_excel=True,
    )

    graphs = []
    # graphs.append(va.generate_merit_order_for(period=5))
    # graphs[0].show()

    # graphs = va.generate_refund_rules_box_plot(periods=list(range(3,30)))
    # graphs[0].show()

    # graphs = va.generate_total_costs_box_plot(periods=list(range(3,120)))
    # graphs[0].show()

    # graphs = va.generate_merit_order_list(periods=[7,8,24])#+list(range(17,25)))
    # graphs = va.generate_merit_order_list(periods=list(range(3, 6)))
    # for graph in graphs:
    #     print(f"graph.data {graph.data}")
    #     print(f"graph.layout {graph.layout}")
    #     print(f"graph.frames {graph.frames}")
    #     graph.show()
    
    # graphs = va.posneg_merit_order_list(periods=list(range(36,40)))
    # for graph in graphs: graph.show() 

    # graphs.append(va.generate_merit_order_slider(periods=list(range(3, 16))))
    # graphs.append(va.generate_soc_plots(periods=list(range(48))))
    # graphs[0].show()

    graph2 = va.generate_soc_plots(periods=list(range(8)))

    # graph0 = va.generate_merit_order_slider(periods=list(range(3, 48)))
    # graphs = [graph0, graph2]
    
    graph0, graph1 = va.posneg_merit_order_slider(periods=list(range(3, 8)))
    graphs = [graph0, graph2, graph1]

    with open(f"pics/figure.html", "w") as file:
        file.write(graphs[0].to_html(include_plotlyjs="cdn"))
        for graph in graphs[1:]:
            file.write(graph.to_html(full_html=False, include_plotlyjs="cdn"))


if __name__ == "__main__":
    main()
